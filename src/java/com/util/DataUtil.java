/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.util;

import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Bao Anh
 */
public class DataUtil {

    public static boolean isNullorEmpty(Object input) {
        if (input == null) {
            return true;
        } else if (input instanceof String && input.toString().trim().isEmpty()) {
            return true;
        }
        return false;
    }

    public static HttpServletResponse setCORSHeaders(Object res) {
        HttpServletResponse response = (HttpServletResponse) res;
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS, DELETE");
        response.addHeader("Access-Control-Allow-Headers", "Content-Type");
        response.addHeader("Access-Control-Max-Age", "86400");
        return response;
    }
}
