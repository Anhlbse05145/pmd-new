/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

/**
 *
 * @author Mi
 */
public class UserRole {
    private int userId;
    private int roleId;
    private String dateCreate;
    private String dateUpdate;

    public UserRole(int userId, int roleId, String dateCreate, String dateUpdate) {
        this.userId = userId;
        this.roleId = roleId;
        this.dateCreate = dateCreate;
        this.dateUpdate = dateUpdate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(String dateUpdate) {
        this.dateUpdate = dateUpdate;
    }
    
    
    
}
