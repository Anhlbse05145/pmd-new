/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

/**
 *
 * @author Bao Anh
 */
public class Result_wait {

    long rw_id;
    long user_id;
    long pma_id;
    String mailTitle;
    long mail_Uid;
    boolean flag;
    long pageNumber;

    public Result_wait() {
    }

    public Result_wait(long rw_id, long user_id, long pma_id, String mailTitle, long mail_Uid, boolean flag) {
        this.rw_id = rw_id;
        this.user_id = user_id;
        this.pma_id = pma_id;
        this.mailTitle = mailTitle;
        this.mail_Uid = mail_Uid;
        this.flag = flag;
    }

    public Result_wait(long user_id, long pageNumber) {
        this.user_id = user_id;
        this.pageNumber = pageNumber;
    }

    public Result_wait(long rw_id, String mailTitle) {
        this.rw_id = rw_id;
        this.mailTitle = mailTitle;
    }

    public Result_wait(long rw_id, boolean flag) {
        this.rw_id = rw_id;
        this.flag = flag;
    }

    public long getRw_id() {
        return rw_id;
    }

    public void setRw_id(long rw_id) {
        this.rw_id = rw_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public long getPma_id() {
        return pma_id;
    }

    public void setPma_id(long pma_id) {
        this.pma_id = pma_id;
    }

    public String getMailTitle() {
        return mailTitle;
    }

    public void setMailTitle(String mailTitle) {
        this.mailTitle = mailTitle;
    }

    public long getMail_Uid() {
        return mail_Uid;
    }

    public void setMail_Uid(long mail_Uid) {
        this.mail_Uid = mail_Uid;
    }

    public boolean getFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public long getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(long pageNumber) {
        this.pageNumber = pageNumber;
    }
}
