/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

/**
 *
 * @author Bao Anh
 */
public class SystemSettting {
    private String fetch_time;
    private String date_create;
    private String date_update;
    private String test_data_size;
    private String retraining_time;

    public SystemSettting(String fetch_time, String date_create, String date_update, String test_data_size, String retraining_time) {
        this.fetch_time = fetch_time;
        this.date_create = date_create;
        this.date_update = date_update;
        this.test_data_size = test_data_size;
        this.retraining_time = retraining_time;
    }

    public SystemSettting(String fetch_time, String test_data_size, String retraining_time) {
        this.fetch_time = fetch_time;
        this.test_data_size = test_data_size;
        this.retraining_time = retraining_time;
    }

    public String getFetch_time() {
        return fetch_time;
    }

    public void setFetch_time(String fetch_time) {
        this.fetch_time = fetch_time;
    }

    public String getDate_create() {
        return date_create;
    }

    public void setDate_create(String date_create) {
        this.date_create = date_create;
    }

    public String getDate_update() {
        return date_update;
    }

    public void setDate_update(String date_update) {
        this.date_update = date_update;
    }

    public String getTest_data_size() {
        return test_data_size;
    }

    public void setTest_data_size(String test_data_size) {
        this.test_data_size = test_data_size;
    }

    public String getRetraining_time() {
        return retraining_time;
    }

    public void setRetraining_time(String retraining_time) {
        this.retraining_time = retraining_time;
    }
    
}
