/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

/**
 *
 * @author Mi
 */
public class UserInfo {

    private int userInfoId;
    private int userId;
    private String userName;
    private String mail;
    private String nation;
    private int mobile;
    private String dateCreate;
    private String dateUpdate;
    private String role;
    private boolean active;

    public UserInfo() {
    }

    public UserInfo(int userInfoId, int userId, String userName, String mail, String nation, int mobile, String dateCreate, String dateUpdate, String role, boolean active) {
        this.userInfoId = userInfoId;
        this.userId = userId;
        this.userName = userName;
        this.mail = mail;
        this.nation = nation;
        this.mobile = mobile;
        this.dateCreate = dateCreate;
        this.dateUpdate = dateUpdate;
        this.role = role;
        this.active = active;
    }

    public UserInfo(int userId, String mail, int mobile, String nation) {
        this.userId = userId;
        this.mail = mail;
        this.mobile = mobile;
        this.nation = nation;
    }

    public int getUserInfoId() {
        return userInfoId;
    }

    public void setUserInfoId(int userInfoId) {
        this.userInfoId = userInfoId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public int getMobile() {
        return mobile;
    }

    public void setMobile(int mobile) {
        this.mobile = mobile;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(String dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
