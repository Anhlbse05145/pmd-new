/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

/**
 *
 * @author Mi-Pc
 */
public class Notification {

    private int id;
    private int user_id;
    private String content;
    private String date_create;
    private String date_update;

    public Notification() {
    }

    public Notification(int id, int user_id, String content, String date_create, String date_update) {
        this.id = id;
        this.user_id = user_id;
        this.content = content;
        this.date_create = date_create;
        this.date_update = date_update;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate_create() {
        return date_create;
    }

    public void setDate_create(String date_create) {
        this.date_create = date_create;
    }

    public String getDate_update() {
        return date_update;
    }

    public void setDate_update(String date_update) {
        this.date_update = date_update;
    }
    
    
}
