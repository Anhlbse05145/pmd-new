/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

/**
 *
 * @author Mi
 */
public class Permission {
    private int permissionId;
    private String title;
    private String dateCreate;
    private String dateUpdate;

    public Permission(int permissionId, String title, String dateCreate, String dateUpdate) {
        this.permissionId = permissionId;
        this.title = title;
        this.dateCreate = dateCreate;
        this.dateUpdate = dateUpdate;
    }

    public int getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(int permissionId) {
        this.permissionId = permissionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(String dateUpdate) {
        this.dateUpdate = dateUpdate;
    }
    
}
