/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

/**
 *
 * @author Mi
 */
public class MailAccount {
    private long pmaID;
    private String dateCreate;
    private String dateUpdate;
    private String mailAccount;
    private String password;
    private long isId;
    private boolean active;
    private long userId;
    private boolean isVerify;

    public MailAccount() {
    }

    public MailAccount(long pmaID, String dateCreate, String dateUpdate, String mailAccount, String password, long isId, boolean active, long userId, boolean isVerify) {
        this.pmaID = pmaID;
        this.dateCreate = dateCreate;
        this.dateUpdate = dateUpdate;
        this.mailAccount = mailAccount;
        this.password = password;
        this.isId = isId;
        this.active = active;
        this.userId = userId;
        this.isVerify = isVerify;
    }

    public MailAccount(long pmaID, String dateCreate, String dateUpdate, String mailAccount, String password, long isId, boolean active) {
        this.pmaID = pmaID;
        this.dateCreate = dateCreate;
        this.dateUpdate = dateUpdate;
        this.mailAccount = mailAccount;
        this.password = password;
        this.isId = isId;
        this.active = active;
    }
    
    public MailAccount(long pmaID, String dateCreate, String dateUpdate, String mailAccount, String password, boolean active, long userId) {
        this.pmaID = pmaID;
        this.dateCreate = dateCreate;
        this.dateUpdate = dateUpdate;
        this.mailAccount = mailAccount;
        this.password = password;
        this.active = active;
        this.userId = userId;
    }

    public long getPmaID() {
        return pmaID;
    }

    public void setPmaID(long pmaID) {
        this.pmaID = pmaID;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(String dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public String getMailAccount() {
        return mailAccount;
    }

    public void setMailAccount(String mailAccount) {
        this.mailAccount = mailAccount;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getIsId() {
        return isId;
    }

    public void setIsId(long isId) {
        this.isId = isId;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
    
        public boolean isIsVerify() {
        return isVerify;
    }

    public void setIsVerify(boolean isVerify) {
        this.isVerify = isVerify;
    }
    
}
