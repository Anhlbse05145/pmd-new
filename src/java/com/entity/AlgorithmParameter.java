/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

/**
 *
 * @author Bao Anh
 */
public class AlgorithmParameter {
    
private  String Time;
private  String Accuracy;
private  String body_html;
private  String url_ports;
private  String subj_reply;
private  String Date_update;
private  String TrueNegative;
private  String TruePositive;
private  String is_multipart;
private  String url_no_links;
private  String url_no_ports;
private  String FalseNegative;
private  String FalsePositive;
private  String body_no_words;
private  String body_richness;
private  String send_no_words;
private  String subj_no_words;
private  String url_at_symbol;
private  String url_link_text;
private  String body_list_word;
private  String subj_list_word;
private  String url_ip_address;
private  String url_no_domains;
private  String have_attachment;
private  String TrueNegativeRate;
private  String TruePositiveRate;
private  String url_no_ext_links;
private  String url_no_img_links;
private  String url_no_int_links;
private  String FalseNegativeRate;
private  String FalsePositiveRate;
private  String presence_form_tag;
private  String body_no_characters;
private  String send_no_characters;
private  String subj_no_characters;
private  String url_max_no_periods;
private  String sender_is_subdomain;
private  String url_no_ip_addresses;
private  String url_non_modal_links;
private  String body_no_distinct_words;
private  String body_no_function_words;
private  String url_based_image_source;
private  String send_diff_sender_reply_to;
private  String send_non_modal_sender_domain;
private  String number_of_linked_to_sender_domain;
private  String sender_domain_not_match_body_domain;

    public AlgorithmParameter() {
    }

    public AlgorithmParameter(String Time, String Accuracy, String body_html, String url_ports, String subj_reply, String Date_update, String TrueNegative, String TruePositive, String is_multipart, String url_no_links, String url_no_ports, String FalseNegative, String FalsePositive, String body_no_words, String body_richness, String send_no_words, String subj_no_words, String url_at_symbol, String url_link_text, String body_list_word, String subj_list_word, String url_ip_address, String url_no_domains, String have_attachment, String TrueNegativeRate, String TruePositiveRate, String url_no_ext_links, String url_no_img_links, String url_no_int_links, String FalseNegativeRate, String FalsePositiveRate, String presence_form_tag, String body_no_characters, String send_no_characters, String subj_no_characters, String url_max_no_periods, String sender_is_subdomain, String url_no_ip_addresses, String url_non_modal_links, String body_no_distinct_words, String body_no_function_words, String url_based_image_source, String send_diff_sender_reply_to, String send_non_modal_sender_domain, String number_of_linked_to_sender_domain, String sender_domain_not_match_body_domain) {
        this.Time = Time;
        this.Accuracy = Accuracy;
        this.body_html = body_html;
        this.url_ports = url_ports;
        this.subj_reply = subj_reply;
        this.Date_update = Date_update;
        this.TrueNegative = TrueNegative;
        this.TruePositive = TruePositive;
        this.is_multipart = is_multipart;
        this.url_no_links = url_no_links;
        this.url_no_ports = url_no_ports;
        this.FalseNegative = FalseNegative;
        this.FalsePositive = FalsePositive;
        this.body_no_words = body_no_words;
        this.body_richness = body_richness;
        this.send_no_words = send_no_words;
        this.subj_no_words = subj_no_words;
        this.url_at_symbol = url_at_symbol;
        this.url_link_text = url_link_text;
        this.body_list_word = body_list_word;
        this.subj_list_word = subj_list_word;
        this.url_ip_address = url_ip_address;
        this.url_no_domains = url_no_domains;
        this.have_attachment = have_attachment;
        this.TrueNegativeRate = TrueNegativeRate;
        this.TruePositiveRate = TruePositiveRate;
        this.url_no_ext_links = url_no_ext_links;
        this.url_no_img_links = url_no_img_links;
        this.url_no_int_links = url_no_int_links;
        this.FalseNegativeRate = FalseNegativeRate;
        this.FalsePositiveRate = FalsePositiveRate;
        this.presence_form_tag = presence_form_tag;
        this.body_no_characters = body_no_characters;
        this.send_no_characters = send_no_characters;
        this.subj_no_characters = subj_no_characters;
        this.url_max_no_periods = url_max_no_periods;
        this.sender_is_subdomain = sender_is_subdomain;
        this.url_no_ip_addresses = url_no_ip_addresses;
        this.url_non_modal_links = url_non_modal_links;
        this.body_no_distinct_words = body_no_distinct_words;
        this.body_no_function_words = body_no_function_words;
        this.url_based_image_source = url_based_image_source;
        this.send_diff_sender_reply_to = send_diff_sender_reply_to;
        this.send_non_modal_sender_domain = send_non_modal_sender_domain;
        this.number_of_linked_to_sender_domain = number_of_linked_to_sender_domain;
        this.sender_domain_not_match_body_domain = sender_domain_not_match_body_domain;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String Time) {
        this.Time = Time;
    }

    public String getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(String Accuracy) {
        this.Accuracy = Accuracy;
    }

    public String getBody_html() {
        return body_html;
    }

    public void setBody_html(String body_html) {
        this.body_html = body_html;
    }

    public String getUrl_ports() {
        return url_ports;
    }

    public void setUrl_ports(String url_ports) {
        this.url_ports = url_ports;
    }

    public String getSubj_reply() {
        return subj_reply;
    }

    public void setSubj_reply(String subj_reply) {
        this.subj_reply = subj_reply;
    }

    public String getDate_update() {
        return Date_update;
    }

    public void setDate_update(String Date_update) {
        this.Date_update = Date_update;
    }

    public String getTrueNegative() {
        return TrueNegative;
    }

    public void setTrueNegative(String TrueNegative) {
        this.TrueNegative = TrueNegative;
    }

    public String getTruePositive() {
        return TruePositive;
    }

    public void setTruePositive(String TruePositive) {
        this.TruePositive = TruePositive;
    }

    public String getIs_multipart() {
        return is_multipart;
    }

    public void setIs_multipart(String is_multipart) {
        this.is_multipart = is_multipart;
    }

    public String getUrl_no_links() {
        return url_no_links;
    }

    public void setUrl_no_links(String url_no_links) {
        this.url_no_links = url_no_links;
    }

    public String getUrl_no_ports() {
        return url_no_ports;
    }

    public void setUrl_no_ports(String url_no_ports) {
        this.url_no_ports = url_no_ports;
    }

    public String getFalseNegative() {
        return FalseNegative;
    }

    public void setFalseNegative(String FalseNegative) {
        this.FalseNegative = FalseNegative;
    }

    public String getFalsePositive() {
        return FalsePositive;
    }

    public void setFalsePositive(String FalsePositive) {
        this.FalsePositive = FalsePositive;
    }

    public String getBody_no_words() {
        return body_no_words;
    }

    public void setBody_no_words(String body_no_words) {
        this.body_no_words = body_no_words;
    }

    public String getBody_richness() {
        return body_richness;
    }

    public void setBody_richness(String body_richness) {
        this.body_richness = body_richness;
    }

    public String getSend_no_words() {
        return send_no_words;
    }

    public void setSend_no_words(String send_no_words) {
        this.send_no_words = send_no_words;
    }

    public String getSubj_no_words() {
        return subj_no_words;
    }

    public void setSubj_no_words(String subj_no_words) {
        this.subj_no_words = subj_no_words;
    }

    public String getUrl_at_symbol() {
        return url_at_symbol;
    }

    public void setUrl_at_symbol(String url_at_symbol) {
        this.url_at_symbol = url_at_symbol;
    }

    public String getUrl_link_text() {
        return url_link_text;
    }

    public void setUrl_link_text(String url_link_text) {
        this.url_link_text = url_link_text;
    }

    public String getBody_list_word() {
        return body_list_word;
    }

    public void setBody_list_word(String body_list_word) {
        this.body_list_word = body_list_word;
    }

    public String getSubj_list_word() {
        return subj_list_word;
    }

    public void setSubj_list_word(String subj_list_word) {
        this.subj_list_word = subj_list_word;
    }

    public String getUrl_ip_address() {
        return url_ip_address;
    }

    public void setUrl_ip_address(String url_ip_address) {
        this.url_ip_address = url_ip_address;
    }

    public String getUrl_no_domains() {
        return url_no_domains;
    }

    public void setUrl_no_domains(String url_no_domains) {
        this.url_no_domains = url_no_domains;
    }

    public String getHave_attachment() {
        return have_attachment;
    }

    public void setHave_attachment(String have_attachment) {
        this.have_attachment = have_attachment;
    }

    public String getTrueNegativeRate() {
        return TrueNegativeRate;
    }

    public void setTrueNegativeRate(String TrueNegativeRate) {
        this.TrueNegativeRate = TrueNegativeRate;
    }

    public String getTruePositiveRate() {
        return TruePositiveRate;
    }

    public void setTruePositiveRate(String TruePositiveRate) {
        this.TruePositiveRate = TruePositiveRate;
    }

    public String getUrl_no_ext_links() {
        return url_no_ext_links;
    }

    public void setUrl_no_ext_links(String url_no_ext_links) {
        this.url_no_ext_links = url_no_ext_links;
    }

    public String getUrl_no_img_links() {
        return url_no_img_links;
    }

    public void setUrl_no_img_links(String url_no_img_links) {
        this.url_no_img_links = url_no_img_links;
    }

    public String getUrl_no_int_links() {
        return url_no_int_links;
    }

    public void setUrl_no_int_links(String url_no_int_links) {
        this.url_no_int_links = url_no_int_links;
    }

    public String getFalseNegativeRate() {
        return FalseNegativeRate;
    }

    public void setFalseNegativeRate(String FalseNegativeRate) {
        this.FalseNegativeRate = FalseNegativeRate;
    }

    public String getFalsePositiveRate() {
        return FalsePositiveRate;
    }

    public void setFalsePositiveRate(String FalsePositiveRate) {
        this.FalsePositiveRate = FalsePositiveRate;
    }

    public String getPresence_form_tag() {
        return presence_form_tag;
    }

    public void setPresence_form_tag(String presence_form_tag) {
        this.presence_form_tag = presence_form_tag;
    }

    public String getBody_no_characters() {
        return body_no_characters;
    }

    public void setBody_no_characters(String body_no_characters) {
        this.body_no_characters = body_no_characters;
    }

    public String getSend_no_characters() {
        return send_no_characters;
    }

    public void setSend_no_characters(String send_no_characters) {
        this.send_no_characters = send_no_characters;
    }

    public String getSubj_no_characters() {
        return subj_no_characters;
    }

    public void setSubj_no_characters(String subj_no_characters) {
        this.subj_no_characters = subj_no_characters;
    }

    public String getUrl_max_no_periods() {
        return url_max_no_periods;
    }

    public void setUrl_max_no_periods(String url_max_no_periods) {
        this.url_max_no_periods = url_max_no_periods;
    }

    public String getSender_is_subdomain() {
        return sender_is_subdomain;
    }

    public void setSender_is_subdomain(String sender_is_subdomain) {
        this.sender_is_subdomain = sender_is_subdomain;
    }

    public String getUrl_no_ip_addresses() {
        return url_no_ip_addresses;
    }

    public void setUrl_no_ip_addresses(String url_no_ip_addresses) {
        this.url_no_ip_addresses = url_no_ip_addresses;
    }

    public String getUrl_non_modal_links() {
        return url_non_modal_links;
    }

    public void setUrl_non_modal_links(String url_non_modal_links) {
        this.url_non_modal_links = url_non_modal_links;
    }

    public String getBody_no_distinct_words() {
        return body_no_distinct_words;
    }

    public void setBody_no_distinct_words(String body_no_distinct_words) {
        this.body_no_distinct_words = body_no_distinct_words;
    }

    public String getBody_no_function_words() {
        return body_no_function_words;
    }

    public void setBody_no_function_words(String body_no_function_words) {
        this.body_no_function_words = body_no_function_words;
    }

    public String getUrl_based_image_source() {
        return url_based_image_source;
    }

    public void setUrl_based_image_source(String url_based_image_source) {
        this.url_based_image_source = url_based_image_source;
    }

    public String getSend_diff_sender_reply_to() {
        return send_diff_sender_reply_to;
    }

    public void setSend_diff_sender_reply_to(String send_diff_sender_reply_to) {
        this.send_diff_sender_reply_to = send_diff_sender_reply_to;
    }

    public String getSend_non_modal_sender_domain() {
        return send_non_modal_sender_domain;
    }

    public void setSend_non_modal_sender_domain(String send_non_modal_sender_domain) {
        this.send_non_modal_sender_domain = send_non_modal_sender_domain;
    }

    public String getNumber_of_linked_to_sender_domain() {
        return number_of_linked_to_sender_domain;
    }

    public void setNumber_of_linked_to_sender_domain(String number_of_linked_to_sender_domain) {
        this.number_of_linked_to_sender_domain = number_of_linked_to_sender_domain;
    }

    public String getSender_domain_not_match_body_domain() {
        return sender_domain_not_match_body_domain;
    }

    public void setSender_domain_not_match_body_domain(String sender_domain_not_match_body_domain) {
        this.sender_domain_not_match_body_domain = sender_domain_not_match_body_domain;
    }


    
}
