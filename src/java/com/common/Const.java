/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.common;

/**
 *
 * @author Bao Anh
 */
public class Const {

    public static final String LOGIN_SUCCESS = "Log In Successful!";
    public static final String LOGIN_FAIL = "Username or password is incorrect!";
    public static final String ACCOUNT_NOT_ACTIVE = "Your account is locked!";
    public static final long TIME_TO_LIVE = 1000000000;
    public static final String ROLE_ADMIN = "admin";
    public static final String ROLE_USER = "user";
    public static final String ACCESS_DENIE = "Access denied!";
    public static final String CANT_GET_DATA = "Can not get data!";
    public static final String UPDATE_SUCCESS = "Update success!";
    public static final String UPDATE_FAIL = "Update fail!";
    public static final String REGISTER_FAIL = "Register fail!";
    public static final String REGISTER_SUCCESS = "Register success!";
    public static final String ACCOUNT_EXISTED = "Account already exists!";
    public static final String INCORRECT_INFORMATION = "Please enter the correct and complete information!";
    public static final String INCORRECT_EMAIL_PHONE = "Incorrect email or phone number format!";
    public static final String INCORRECT_PASSWORD_FORMAT = "Incorrect Password format!";
    public static final String CONTAIN_SPE_CHAR = "User Name have not special character!";
    public static final String PASSWORD_NOT_WORK = "ERROR: Password does not work, please reenter";
    public static final String CHANGE_PASS_SUCCESS = "Change password success!";
    public static final String INVALID_CREDENTIALS = "Invalid Credentials provided!";
    public static final String RESET_SUCCESS = "Reset success!";
    public static final String RESET_FAIL = "Reset fail!";
    public static final String FORGET_PASS_TITLE = "New password from PMD";
    public static final String NEW_PASSWORD_IS = "Your new password is: ";
    public static final String GET_DATA_SUCCESS = "Get data success!";
    public static final String CREATE_FAIL = "Create fail!";
    public static final String CREATE_SUCCESS = "Create success!";
}
