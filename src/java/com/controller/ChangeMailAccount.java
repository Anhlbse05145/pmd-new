/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.entity.MailAccount;
import com.entity.UserAccount;
import com.entity.UserInfo;
import com.model.CheckFrom;
import com.model.CookieLogin;
import com.model.DataLogin;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mi-Pc
 */
@WebServlet(name = "ChangeMailAccount", urlPatterns = {"/changeMailAccount"})
public class ChangeMailAccount extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public ChangeMailAccount() {
        super();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        RequestDispatcher dispatcher //
                = this.getServletContext().getRequestDispatcher("/WEB-INF/views/changeMailAccount.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pmaID = (String) request.getParameter("pmaID");
        String mail = (String) request.getParameter("mail");
        String password = (String) request.getParameter("password");
        String mailServer = (String) request.getParameter("mailServer");
        String active = (String) request.getParameter("active");
        String errorString = null;
        if (mail.isEmpty() || password.isEmpty() || mailServer.isEmpty()) {
            errorString = "Please fill in the correct and complete information";
            request.setAttribute("errorString", errorString);
            // 
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/changeMailAccount.jsp");
            dispatcher.forward(request, response);
        }
        if ((CheckFrom.isValidMail(mail) == false)) {
            errorString = "Incorrect email format!!!";
            request.setAttribute("errorString", errorString);
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/changeMailAccount.jsp");
            dispatcher.forward(request, response);
        } else {
            try {
                 DataLogin.editPersonalMailAccount(pmaID, mail, password, mailServer, active);
            } catch (SQLException ex) {
                Logger.getLogger(ChangeMailAccount.class.getName()).log(Level.SEVERE, null, ex);
            }
            response.sendRedirect(request.getContextPath() + "/listMailAccount");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
