/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.model.Widgets;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mi
 */
@WebServlet(name = "home", urlPatterns = { "/home"})
public class HomeServlet extends HttpServlet {
   private static final long serialVersionUID = 1L;
 
   public HomeServlet() {
       super();
   }
 
   @Override
   protected void doGet(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {
 
       // Forward toi trang /WEB-INF/views/homeView.jsp
       // (Người dùng không bao giờ truy cập trực tiếp được vào các trang JSP
       // đặt trong WEB-INF)
       String gold = "gold";
       String silver = "silver";
       String platinum = "platinum";
       try {
           Widgets widgets = new Widgets();
           request.setAttribute("totalUser", widgets.countUser());
           request.setAttribute("totalUserGold", widgets.countRoleName(gold));
           request.setAttribute("totalUserSilver", widgets.countRoleName(silver));
           request.setAttribute("totalUserPlatinum", widgets.countRoleName(platinum));
           request.setAttribute("countUserNew", widgets.countUserNew());
           request.setAttribute("countMail", widgets.countMail());
           request.setAttribute("countMailToDay", widgets.countMailToDay());
           request.getRequestDispatcher("/WEB-INF/views/homeView.jsp").forward(request, response); 
       } catch (Exception e) {
//           RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/homeView.jsp");
//           dispatcher.forward(request, response);
           Logger.getLogger(HomeServlet.class.getName()).log(Level.SEVERE, null, e);
       }
       
        
   }
 
   @Override
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
           throws ServletException, IOException {
       doGet(request, response);
   }

}
