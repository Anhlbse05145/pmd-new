/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.common.Const;
import com.dto.UpdateSystemSettingdto;
import com.entity.UserAccount;
import com.google.gson.Gson;
import com.model.CookieLogin;
import com.model.DataLogin;
import java.io.IOException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Bao Anh
 */
@WebServlet(name = "UpdateSystemSetting", urlPatterns = {"/UpdateSystemSetting"})
public class UpdateSystemSettingAPI extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String errorString = "";
        try {
            Gson gson = new Gson();
            HttpSession session = request.getSession();
            UserAccount loginedUser = CookieLogin.getLoginedUser(session);
            Connection conn = CookieLogin.getStoredConnection(request);
            String setting = request.getParameter("setting");
            UpdateSystemSettingdto settingdto = gson.fromJson(setting, UpdateSystemSettingdto.class);
            String checkRole = DataLogin.roleUser(loginedUser.getUserName());
            int result = 0;
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
            Date date = new Date();
            String dateUpdate = format.format(date);
            String json = "\"fetch_time\"=>\"" + settingdto.getFetch_time()+ "\", \"date_update\"=>\"" +dateUpdate+ "\", \"test_data_size\"=>\"" +settingdto.getTest_data_size()+ "\", \"retraining_time\"=>\"" +settingdto.getRetraining_time()+ "\""  ;
            if(checkRole.equals(Const.ROLE_ADMIN)){
                result = DataLogin.updateSystemSetting(conn, json);
                if(result > 0){
                    errorString = Const.UPDATE_SUCCESS;
                } else{
                    errorString = Const.UPDATE_FAIL;
                }
            } else{
                errorString = Const.ACCESS_DENIE;
            }
        } catch(Exception ex){
            ex.printStackTrace();
            errorString = Const.UPDATE_FAIL;
        }
        
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(errorString);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
