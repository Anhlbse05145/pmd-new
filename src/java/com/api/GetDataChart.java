/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.common.Const;
import com.dto.DataChartdto;
import com.entity.AlgorithmParameter;
import com.entity.UserAccount;
import com.google.gson.Gson;
import com.model.CookieLogin;
import com.model.DataLogin;
import java.io.IOException;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Bao Anh
 */
@WebServlet(name = "GetDataChart", urlPatterns = {"/GetDataChart"})
public class GetDataChart extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String errorString = "";
        AlgorithmParameter ap = null;
        DataChartdto aParameter = new DataChartdto();
        String jsonRes = "";
        try {
            Gson gson = new Gson();
            HttpSession session = request.getSession();
            Connection conn = CookieLogin.getStoredConnection(request);
            
            UserAccount loginedUser = CookieLogin.getLoginedUser(session);
            String checkRole = "";
            checkRole = DataLogin.roleUser(loginedUser.getUserName());
            if (!Const.ROLE_ADMIN.equals(checkRole)) {
                errorString = Const.ACCESS_DENIE;
            } else {
                ap = DataLogin.getDataChart(conn);
                aParameter.setBody_list_word(Double.parseDouble(ap.getBody_list_word()));
                aParameter.setBody_html(Double.parseDouble(ap.getBody_html()));
                aParameter.setNumber_of_linked_to_sender_domain(Double.parseDouble(ap.getNumber_of_linked_to_sender_domain()));
                aParameter.setSender_domain_not_match_body_domain(Double.parseDouble(ap.getSender_domain_not_match_body_domain()));
                aParameter.setPresence_form_tag(Double.parseDouble(ap.getPresence_form_tag()));
                aParameter.setUrl_based_image_source(Double.parseDouble(ap.getUrl_based_image_source()));
                aParameter.setBody_no_characters(Double.parseDouble(ap.getBody_no_characters()));
                aParameter.setBody_no_distinct_words(Double.parseDouble(ap.getBody_no_distinct_words()));
                aParameter.setBody_no_function_words(Double.parseDouble(ap.getBody_no_function_words()));
                aParameter.setBody_no_words(Double.parseDouble(ap.getBody_no_words()));
                aParameter.setBody_richness(Double.parseDouble(ap.getBody_richness()));
                aParameter.setSender_is_subdomain(Double.parseDouble(ap.getSender_is_subdomain()));
                aParameter.setSend_no_characters(Double.parseDouble(ap.getSend_no_characters()));
                aParameter.setSend_no_words(Double.parseDouble(ap.getSend_no_words()));
                aParameter.setSend_diff_sender_reply_to(Double.parseDouble(ap.getSend_diff_sender_reply_to()));
                aParameter.setSend_non_modal_sender_domain(Double.parseDouble(ap.getSend_non_modal_sender_domain()));
                aParameter.setSubj_list_word(Double.parseDouble(ap.getSubj_list_word()));
                aParameter.setSubj_reply(Double.parseDouble(ap.getSubj_reply()));
                aParameter.setSubj_no_words(Double.parseDouble(ap.getSubj_no_words()));
                aParameter.setSubj_no_characters(Double.parseDouble(ap.getSubj_no_characters()));
                aParameter.setUrl_at_symbol(Double.parseDouble(ap.getUrl_at_symbol()));
                aParameter.setUrl_ip_address(Double.parseDouble(ap.getUrl_ip_address()));
                aParameter.setUrl_link_text(Double.parseDouble(ap.getUrl_link_text()));
                aParameter.setUrl_no_domains(Double.parseDouble(ap.getUrl_no_domains()));
                aParameter.setUrl_max_no_periods(Double.parseDouble(ap.getUrl_max_no_periods()));
                aParameter.setUrl_no_ext_links(Double.parseDouble(ap.getUrl_no_ext_links()));
                aParameter.setUrl_no_links(Double.parseDouble(ap.getUrl_no_links()));
                aParameter.setUrl_no_int_links(Double.parseDouble(ap.getUrl_no_int_links()));
                aParameter.setUrl_no_ip_addresses(Double.parseDouble(ap.getUrl_no_ip_addresses()));
                aParameter.setUrl_no_img_links(Double.parseDouble(ap.getUrl_no_img_links()));
                aParameter.setUrl_no_ports(Double.parseDouble(ap.getUrl_no_ports()));
                aParameter.setUrl_non_modal_links(Double.parseDouble(ap.getUrl_non_modal_links()));
                aParameter.setUrl_ports(Double.parseDouble(ap.getUrl_ports()));
                aParameter.setIs_multipart(Double.parseDouble(ap.getIs_multipart()));
                aParameter.setHave_attachment(Double.parseDouble(ap.getHave_attachment()));
                
                jsonRes = gson.toJson(aParameter);
                errorString = Const.GET_DATA_SUCCESS;
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }
        
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonRes);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
