/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.common.Const;
import com.entity.UserAccount;
import com.model.CookieLogin;
import com.model.DataLogin;
import java.io.IOException;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Bao Anh
 */
@WebServlet(name = "ResetPasswordAPI", urlPatterns = {"/ResetPasswordAPI"})
public class ResetPasswordAPI extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String errorString = "";
        try {
            Connection conn = CookieLogin.getStoredConnection(request);
            HttpSession session = request.getSession();
            // Kiểm tra người dùng đã đăng nhập (login) chưa.
            UserAccount loginedUser = CookieLogin.getLoginedUser(session);

            String userId = (String) request.getParameter("userId");

            String checkRole = "";
            boolean checkActive = false;
            int result = 0;

            checkActive = DataLogin.checkActive(conn, loginedUser.getUserName(), loginedUser.getPassword());
            if (checkActive) {
                checkRole = DataLogin.roleUser(loginedUser.getUserName());
                if (!Const.ROLE_ADMIN.equals(checkRole) || checkActive == false) {
                    errorString = Const.ACCESS_DENIE;
                } else {
                    result = DataLogin.resetPassword(conn, userId);
                    if (result != 0) {
                        errorString = Const.RESET_SUCCESS;
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            errorString = Const.RESET_FAIL;
        }

        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(errorString);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
