/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.common.Const;
import com.dto.NewMailAccountdto;
import com.entity.UserAccount;
import com.google.gson.Gson;
import com.model.CheckFrom;
import com.model.CookieLogin;
import com.model.DataLogin;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Bao Anh
 */
@WebServlet(name = "newMaillAccountAPI", urlPatterns = {"/newMaillAccountAPI"})
public class newMaillAccountAPI extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String errorString = "";
        try {
            Gson gson = new Gson();
            String jsonInput = request.getParameter("newMailAccount");
            NewMailAccountdto mailAccountdto = gson.fromJson(jsonInput, NewMailAccountdto.class);
            HttpSession session = request.getSession();
            UserAccount loginedUser = CookieLogin.getLoginedUser(session);
            String mail = mailAccountdto.getMail();
            String mailPass = mailAccountdto.getMailPass();
            String accPass = mailAccountdto.getAccPass();
            String mailIsId = mailAccountdto.getMailIsId();
            String active = mailAccountdto.getActive();
            int result = 0;
            if (mail.isEmpty() || mailPass.isEmpty() || accPass.isEmpty() || mailIsId.isEmpty() || active.isEmpty()) {
                errorString = "Please fill in the correct and complete information";
            } else if ((CheckFrom.isValidMail(mail) == false)) {
                errorString = "Incorrect email format!!!";
            } else if (!loginedUser.getPassword().equals(accPass)) {
                errorString = Const.ACCESS_DENIE;
            } else {
                result = DataLogin.cratePersonalMailAccount(loginedUser.getUserName(), mail, mailPass, mailIsId, active);
                if(result > 0){
                    errorString = Const.CREATE_SUCCESS;
                } else{
                    errorString = Const.CREATE_FAIL;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            errorString = Const.CREATE_FAIL;
        }
        
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(errorString);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
