package com.api;


import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

public class EmitLogDirect {

    private static final String EXCHANGE_NAME = "knistro";
    private static boolean durable = true;
    private static String severity = "final_action";

    public void sendMessageRabbit(String message) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("150.95.104.39");
        try (Connection connection = factory.newConnection();
                Channel channel = connection.createChannel()) {
            channel.exchangeDeclare(EXCHANGE_NAME, "direct", durable);

            channel.basicPublish(EXCHANGE_NAME, severity, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes("UTF-8"));
            System.out.println(" [x] Sent '" + severity + "':'" + message + "'");
        }
    }
}
