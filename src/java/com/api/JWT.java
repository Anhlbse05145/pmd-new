/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import it.cosenonjaviste.security.jwt.utils.JwtTokenBuilder;
import it.cosenonjaviste.security.jwt.utils.JwtTokenVerifier;
import java.util.Arrays;
import java.util.Date;

/**
 *
 * @author Bao Anh
 */
public class JWT {

    public static final String SECRET_KEY = "AAAA";

    public static String encodeJWT(String userName, String userPW, long ttlMillis) throws Exception {
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        int expMillis = Integer.parseInt(Long.toString((nowMillis + ttlMillis) / 1000));
        JwtTokenBuilder tokenBuilder = JwtTokenBuilder.create(SECRET_KEY);
        String token = tokenBuilder.userId(userName).claimEntry("userName", userName)
                .claimEntry("password", userPW)
                .expirySecs(expMillis).roles(Arrays.asList("admin", "user")).build();

        return token;
    }

    public static boolean isVerify(String token) {
        JwtTokenVerifier verifier = JwtTokenVerifier.create(SECRET_KEY);
        boolean check = verifier.verify(token);
        return check;
    }

}
