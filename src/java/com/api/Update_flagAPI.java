/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.dto.PMAdto;
import com.dto.Push_Result_waitdto;
import com.entity.Result_wait;
import com.google.gson.Gson;
import com.model.Api;
import com.model.CookieLogin;
import com.model.DataLogin;
import java.io.IOException;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Bao Anh
 */
@WebServlet(name = "Update_flag", urlPatterns = {"/Update_flag"})
public class Update_flagAPI extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.addHeader("Access-Control-Allow-Origin", "*");
        String json = "";
        Push_Result_waitdto rw = null;
        PMAdto pMAdto = null;
        EmitLogDirect direct = new EmitLogDirect();
        Result_wait result_w = null;
        try {
            Gson gson = new Gson();
            Connection conn = CookieLogin.getStoredConnection(request);
            String data = request.getParameter("rwInfo");
            Push_Result_waitdto result_wait = gson.fromJson(data, Push_Result_waitdto.class);
            rw = Api.updateRwMail(conn, result_wait.getRw_id(), result_wait.isFlag());
            if (rw != null) {
                json = gson.toJson(rw);
                result_w = DataLogin.findRw(conn, result_wait.getRw_id());
                pMAdto = DataLogin.getPMA(conn, result_wait.getRw_id());
                direct.sendMessageRabbit(pMAdto.getUserName() + "|" + pMAdto.getPassword()
                        + "|" + pMAdto.getMailServer() + "|" + result_w.getMail_Uid() + "|" + result_w.getFlag());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
