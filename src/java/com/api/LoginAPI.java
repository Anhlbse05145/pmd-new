/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.common.Const;
import com.dto.Login;
import com.entity.UserAccount;
import com.google.gson.Gson;
import com.model.CookieLogin;
import com.model.DataLogin;
import com.util.DataUtil;
import java.io.IOException;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author Bao Anh
 */
@WebServlet(name = "LoginAPI", urlPatterns = {"/LoginAPI"})
public class LoginAPI extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.addHeader("Access-Control-Allow-Origin", "*");
        String token = "";
        boolean loggedIn = false;
        String userId = "";
        String message = "";
        boolean checkActive = false;
        UserAccount userAccount = null;
        UserAccount userAccount1 = null;
        Login login = null;
        String json = "";
        try {
            //Get userName and password from request
            Gson gson = new Gson();
            String jsonUser = request.getParameter("userInfo");
            String role = "";
            if (!DataUtil.isNullorEmpty(jsonUser)) {
                userAccount = gson.fromJson(jsonUser, UserAccount.class);
                String userName = userAccount.getUserName().toLowerCase();
                String password = userAccount.getPassword();
                HttpSession session = request.getSession();
                session.setMaxInactiveInterval(Integer.parseInt(Long.toString(Const.TIME_TO_LIVE)));
                //Find UserAccount
                Connection conn = CookieLogin.getStoredConnection(request);
                checkActive = DataLogin.checkActive(conn, userName, password);

                userAccount1 = DataLogin.findUser(conn, userName.toLowerCase(), password);
                
                // Set time to live for token
                long ttlMillis = Const.TIME_TO_LIVE;

                if (!DataUtil.isNullorEmpty(userAccount1) && userAccount1.getUserName().equals(userName)
                        && BCrypt.checkpw(password,userAccount1.getPassword())) {
                    if (checkActive) {
                        role = DataLogin.roleUser(userName);
                        token = JWT.encodeJWT(userName, password, ttlMillis);
                        message = Const.LOGIN_SUCCESS;
                        CookieLogin.storeLoginedUser(session, userAccount1);
                        loggedIn = true;
                        userId = Integer.toString(userAccount1.getUserId());
                        login = new Login(loggedIn, message, token, userId, role);
                    } else {
                        token = "";
                        loggedIn = false;
                        message = Const.ACCOUNT_NOT_ACTIVE;
                        login = new Login(loggedIn, message, token, userId, role);
                    }
                } else {
                    token = "";
                    loggedIn = false;
                    message = Const.LOGIN_FAIL;
                    login = new Login(loggedIn, message, token, userId, role);
                }
            }
            json = gson.toJson(login);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
