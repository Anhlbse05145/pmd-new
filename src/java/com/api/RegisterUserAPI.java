/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.common.Const;
import com.dto.UserRegisterdto;
import com.google.gson.Gson;
import com.model.CheckFrom;
import com.model.DataLogin;
import com.util.DataUtil;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author Bao Anh
 */
@WebServlet(name = "RegisterUser", urlPatterns = {"/RegisterUser"})
public class RegisterUserAPI extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userName = "";
        String password = "";
        String confirmPassword = "";
        String mail = "";
        String mobile = "";
        String nation = "";
        String errorString = "";
        String userRegisterInput = "";
        Gson gson = new Gson();
        UserRegisterdto registerdto = null;
        try {
            userRegisterInput = request.getParameter("userRegister");
            registerdto = gson.fromJson(userRegisterInput, UserRegisterdto.class);
            userName = registerdto.getUserName();
            password = registerdto.getPassword();
            confirmPassword = registerdto.getConfirmPassword();
            mail = registerdto.getMail();
            mobile = registerdto.getMobile();
            nation = registerdto.getNation();

            if (DataUtil.isNullorEmpty(userName) || DataUtil.isNullorEmpty(password)
                    || DataUtil.isNullorEmpty(confirmPassword) || DataUtil.isNullorEmpty(mail)
                    || DataUtil.isNullorEmpty(mobile) || DataUtil.isNullorEmpty(nation)
                    || !password.equals(confirmPassword)) {
                errorString = Const.INCORRECT_INFORMATION;
            }
            if (!CheckFrom.isValidMail(mail) || !CheckFrom.isValidPhone(mobile)) {
                errorString = Const.INCORRECT_EMAIL_PHONE;
            }
            if (!CheckFrom.isValidPassword(password)) {
                errorString = Const.INCORRECT_PASSWORD_FORMAT;
            }
            if ((CheckFrom.isValidSpecial(userName) == false)) {
                errorString = Const.CONTAIN_SPE_CHAR;
            } else {
                int idCheck;
                try {
                    idCheck = DataLogin.checkName(userName);
                    if (idCheck == 0 && CheckFrom.isValidPassword(password) && CheckFrom.isValidSpecial(userName)) {
                        try {
                            //Đăng ký acc
                            DataLogin.registerUser(userName.toLowerCase(), BCrypt.hashpw(password, BCrypt.gensalt(12)));
                            //Đăng ký info
                            DataLogin.registerInfoUser(userName.toLowerCase().trim(), mail, mobile, nation);
                            //Đăng ký role_id
                            DataLogin.registerRoleId(userName);
                            //Đăng ký role
                            DataLogin.registerRoleName(userName);
                            
                            errorString = Const.REGISTER_SUCCESS;
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                            errorString = Const.REGISTER_FAIL;
                        }
                    } else {
                        errorString = Const.ACCOUNT_EXISTED;
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    errorString = Const.REGISTER_FAIL;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            errorString = Const.REGISTER_FAIL;
        }
        
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(errorString);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
