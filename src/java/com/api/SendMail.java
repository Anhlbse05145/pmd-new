/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.sun.mail.smtp.SMTPTransport;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

/**
 *
 * @author Bao Anh
 */
public class SendMail {

    public static void sendMail(String emailSubject, String emailText, String emailTo) throws MessagingException {
        
        ResourceBundle rb = ResourceBundle.getBundle("com.properties.Config");
        String SMTP_SERVER = rb.getString("SMTP_SERVER");
        String PORT = rb.getString("PORT");
        String PASSWORD = rb.getString("PASSWORD");
        String EMAIL_FROM = rb.getString("EMAIL_FROM");
        
        Properties prop = System.getProperties();
        prop.put("mail.smtp.host", SMTP_SERVER); //optional, defined in SMTPTransport
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.port", PORT); // default port 25
        prop.put("mail.smtp.starttls.enable", "true");
        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(EMAIL_FROM, PASSWORD);
                    }
                });
        Message msg = new MimeMessage(session);

        try {
            // from
            msg.setFrom(new InternetAddress(EMAIL_FROM));
            // to 
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(emailTo, false));
            // subject
            msg.setSubject(emailSubject);
            // content 
            msg.setText(emailText);
            msg.setSentDate(new Date());
            // Get SMTPTransport
            SMTPTransport t = (SMTPTransport) session.getTransport("smtp");
            // connect
            t.connect(SMTP_SERVER, EMAIL_FROM, PASSWORD);
            // send
            t.sendMessage(msg, msg.getAllRecipients());
            System.out.println("Response: " + t.getLastServerResponse());
            t.close();
        } catch (MessagingException e) {
            e.printStackTrace();
            throw e;
        }
    }

}
