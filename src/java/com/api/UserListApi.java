/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.common.Const;
import com.dto.UserInfodto;
import com.entity.UserAccount;
import com.entity.UserInfo;
import com.google.gson.Gson;
import com.model.CookieLogin;
import com.model.DataLogin;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Bao Anh
 */
@WebServlet(name = "UserListApi", urlPatterns = {"/UserListApi"})
public class UserListApi extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String errorString = "";
        List<UserInfo> list = null;
        String jsonRes = "";
        try {
            Gson gson = new Gson();
            HttpSession session = request.getSession();
            Connection conn = CookieLogin.getStoredConnection(request);

            String checkRole;

            // Kiểm tra người dùng đã đăng nhập (login) chưa.
            UserAccount loginedUser = CookieLogin.getLoginedUser(session);

            //TODO test
            loginedUser = new UserAccount("admin", "12345678");

            // Nếu chưa đăng nhập (login).
            if (loginedUser == null) {
                // Redirect (Chuyển hướng) tới trang login.
                response.sendRedirect(request.getContextPath() + "/login");
                return;
            }

            //Only role admin can get data from db
            try {
                checkRole = DataLogin.roleUser(loginedUser.getUserName());
                if (!Const.ROLE_ADMIN.equals(checkRole)) {
                    errorString = Const.ACCESS_DENIE;
                } else {
                    list = DataLogin.queryUserInfo(conn);
                }
            } catch (SQLException e) {
                errorString = Const.CANT_GET_DATA;
            }

            jsonRes = gson.toJson(new UserInfodto(list, errorString));

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonRes);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
