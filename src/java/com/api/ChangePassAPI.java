/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.common.Const;
import com.dto.ChangePassdto;
import com.entity.UserAccount;
import com.google.gson.Gson;
import com.model.CookieLogin;
import com.model.DataLogin;
import com.util.DataUtil;
import java.io.IOException;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Bao Anh
 */
@WebServlet(name = "ChangePassAPI", urlPatterns = {"/ChangePassAPI"})
public class ChangePassAPI extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String errorString = "";
        try {
            Gson gson = new Gson();
            Connection conn = CookieLogin.getStoredConnection(request);
            HttpSession session = request.getSession();
            UserAccount loginedUser = CookieLogin.getLoginedUser(session);
            String userName = loginedUser.getUserName();
            String password = loginedUser.getPassword();
            String changePassInput = request.getParameter("changePass");
            ChangePassdto changePassdto = gson.fromJson(changePassInput, ChangePassdto.class);
            String currentPassword = changePassdto.getCurrentPassword();
            String newPassword = changePassdto.getNewPassword();
            String confirmPassword = changePassdto.getConfirmPassword();
            
            if (DataUtil.isNullorEmpty(confirmPassword) || DataUtil.isNullorEmpty(newPassword) || DataUtil.isNullorEmpty(currentPassword)
                    || newPassword.equals(currentPassword) || !newPassword.equals(confirmPassword) || !password.equals(currentPassword)) {
                errorString = Const.PASSWORD_NOT_WORK;
            } else {
                String note = DataLogin.changePassword(conn, userName, currentPassword, newPassword);
                errorString = note;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            errorString = Const.UPDATE_FAIL;
        }
        
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(errorString);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
