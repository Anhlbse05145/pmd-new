/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.common.Const;
import com.dto.UserAccountdto;
import com.entity.UserAccount;
import com.google.gson.Gson;
import com.model.CookieLogin;
import com.model.DataLogin;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Bao Anh
 */
@WebServlet(name = "UpdateActiveAPI", urlPatterns = {"/UpdateActiveAPI"})
public class UpdateActiveAPI extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String errorString = "";
        int row = 0;
        try {
            String jsonInput = request.getParameter("userAccount");
            Gson gson = new Gson();
            HttpSession session = request.getSession();

            String checkRole;

            // Kiểm tra người dùng đã đăng nhập (login) chưa.
            UserAccount loginedUser = CookieLogin.getLoginedUser(session);
            UserAccountdto accountdto = gson.fromJson(jsonInput, UserAccountdto.class);

            //Only role admin can get data from db
            try {
                checkRole = DataLogin.roleUser(loginedUser.getUserName());
                if (!Const.ROLE_ADMIN.equals(checkRole)) {
                    errorString = Const.ACCESS_DENIE;
                } else {
                    row = DataLogin.editActive(accountdto.getUserId(), accountdto.isActive());
                }
            } catch (SQLException e) {
                errorString = Const.CANT_GET_DATA;
            }
        } catch(Exception ex){
            ex.printStackTrace();
            errorString = Const.UPDATE_FAIL;
        }
        
        if(row > 0){
            errorString = Const.UPDATE_SUCCESS;
        } else{
            errorString = Const.UPDATE_FAIL;
        }
        
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(errorString);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
