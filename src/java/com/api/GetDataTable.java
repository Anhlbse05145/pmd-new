/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.common.Const;
import com.dto.DataTabledto;
import com.dto.ListDataTabledto;
import com.entity.AlgorithmParameter;
import com.entity.UserAccount;
import com.google.gson.Gson;
import com.model.CookieLogin;
import com.model.DataLogin;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Bao Anh
 */
@WebServlet(name = "GetDataTable", urlPatterns = {"/GetDataTable"})
public class GetDataTable extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String errorString = "";
        AlgorithmParameter ap = null;
        DataTabledto dataTabledto = new DataTabledto();
        ListDataTabledto ldt = null;
        String jsonRes = "";
        try {
            Gson gson = new Gson();
            HttpSession session = request.getSession();
            Connection conn = CookieLogin.getStoredConnection(request);

            UserAccount loginedUser = CookieLogin.getLoginedUser(session);
            String checkRole = "";
            checkRole = DataLogin.roleUser(loginedUser.getUserName());
            if (!Const.ROLE_ADMIN.equals(checkRole)) {
                errorString = Const.ACCESS_DENIE;
            } else {
                ap = DataLogin.getDataChart(conn);
                dataTabledto.setTruePositive(Double.parseDouble(ap.getTruePositive()));
                dataTabledto.setTrueNegative(Double.parseDouble(ap.getTrueNegative()));
                dataTabledto.setFalseNegative(Double.parseDouble(ap.getFalseNegative()));
                dataTabledto.setFalsePositive(Double.parseDouble(ap.getFalsePositive()));
                dataTabledto.setFalsePositiveRate(Double.parseDouble(ap.getFalsePositiveRate()));
                dataTabledto.setFalseNegativeRate(Double.parseDouble(ap.getFalseNegativeRate()));
                dataTabledto.setTruePositiveRate(Double.parseDouble(ap.getTruePositiveRate()));
                dataTabledto.setTrueNegativeRate(Double.parseDouble(ap.getTrueNegativeRate()));
                dataTabledto.setTime(Double.parseDouble(ap.getTime()));
                dataTabledto.setAccuracy(Double.parseDouble(ap.getAccuracy()));
                
                List<DataTabledto> listAP = new ArrayList<DataTabledto>();
                listAP.add(dataTabledto);
                errorString = Const.GET_DATA_SUCCESS;
                ldt = new ListDataTabledto(listAP, errorString);
                
                jsonRes = gson.toJson(ldt);
                
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonRes);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
