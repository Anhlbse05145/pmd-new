/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.common.Const;
import com.dto.Roledto;
import com.entity.UserAccount;
import com.google.gson.Gson;
import com.model.CookieLogin;
import com.model.DataLogin;
import com.util.DataUtil;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Bao Anh
 */
@WebServlet(name = "UpdateRoleAPI", urlPatterns = {"/UpdateRoleAPI"})
public class UpdateRoleAPI extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String errorString = "";
        try {
            Gson gson = new Gson();
            String roleStr = request.getParameter("userRole");
            Roledto roledto = null;
            HttpSession session = request.getSession();
            
            if (!DataUtil.isNullorEmpty(roleStr)) {
                roledto = gson.fromJson(roleStr, Roledto.class);
            }
            
            // Check user logged in???
            UserAccount loginedUser = CookieLogin.getLoginedUser(session);
            
            String checkRole;
            int result = 0;
            // Check role
            checkRole = DataLogin.roleUser(loginedUser.getUserName());
            if (Const.ROLE_ADMIN.equals(checkRole)) {
                result = DataLogin.editRole(roledto.getUserName(), roledto.getRole());
                if(result > 0){
                    errorString = Const.UPDATE_SUCCESS;
                } else{
                    errorString = Const.UPDATE_FAIL;
                }
            } else {
                errorString = Const.ACCESS_DENIE;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            errorString = Const.UPDATE_FAIL;
        }
        
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(errorString);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
