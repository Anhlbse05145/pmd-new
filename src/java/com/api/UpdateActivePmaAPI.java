/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api;

import com.common.Const;
import com.dto.UpdateActivePMAdto;
import com.entity.MailAccount;
import com.entity.UserAccount;
import com.google.gson.Gson;
import com.model.CookieLogin;
import com.model.DataLogin;
import java.io.IOException;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Bao Anh
 */
@WebServlet(name = "UpdateActivePmaAPI", urlPatterns = {"/UpdateActivePmaAPI"})
public class UpdateActivePmaAPI extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String errorString = "";
        int row = 0;
        try {
            String jsonInput = request.getParameter("updateActivePMA");
            Gson gson = new Gson();
            HttpSession session = request.getSession();
            Connection conn = CookieLogin.getStoredConnection(request);

            // Kiểm tra người dùng đã đăng nhập (login) chưa.
            UserAccount loginedUser = CookieLogin.getLoginedUser(session);
            UpdateActivePMAdto updateActivePMAdto = gson.fromJson(jsonInput, UpdateActivePMAdto.class);

            MailAccount mailAccount = DataLogin.getPMAfromID(conn, updateActivePMAdto.getPmaId());

            String pmaId = updateActivePMAdto.getPmaId();
            String active = updateActivePMAdto.getActive();
            row = DataLogin.editActivePMA(pmaId, active);

        } catch (Exception ex) {
            ex.printStackTrace();

        }

        if (row > 0) {
            errorString = Const.UPDATE_SUCCESS;
        } else {
            errorString = Const.UPDATE_FAIL;
        }

        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(errorString);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
