/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filter;

import com.api.JWT;
import com.entity.UserAccount;
import com.model.CookieLogin;
import com.model.DataLogin;
import com.util.DataUtil;
import it.cosenonjaviste.security.jwt.utils.JwtTokenVerifier;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mi
 */
public class CookieFilter implements Filter {

    private final static String typeExtension = "extension";
    private final static String typeWeb = "web";

    public CookieFilter() {
    }

    @Override
    public void init(FilterConfig fConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        
        HttpSession session = req.getSession();
        String userToken = req.getParameter("token");
            try {
                if (!DataUtil.isNullorEmpty(userToken)) {

                    if(JWT.isVerify(userToken)){
                        chain.doFilter(request, response);
                    } else {
                        req.getRequestDispatcher("ErrorLoginAPI").forward(request, response);
                    }

                }
            } catch (Exception ex) {
                req.getRequestDispatcher("ErrorLoginAPI").forward(request, response);
            }
    }

}
