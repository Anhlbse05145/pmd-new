/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import com.entity.UserAccount;
import com.entity.UserInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.connect.DBContext;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.security.SecureRandom;
import java.sql.Date;

/**
 *
 * @author Mi-Pc
 */
public class Widgets {

    private static java.sql.Date convertUtilToSql(java.util.Date uDate) {
        java.sql.Date sDate = new java.sql.Date(uDate.getTime());
        return sDate;
    }

    private final DBContext db;

    public Widgets() throws Exception {
        db = new DBContext();
    }
    
    public static int countMailToDay() throws SQLException {
        java.util.Date uDate = new java.util.Date();
        java.sql.Date sDate = convertUtilToSql(uDate);
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = " SELECT count(create_date)\n"
                + " FROM public.mail_attribute where create_date = ? ";
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            ps.setDate(1, sDate);
            rs = ps.executeQuery();
            if (rs.next()) {
                int countMail = rs.getInt(1);
                return countMail;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return 0;
    }

    public static int countMail() throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = " SELECT count(attribute_id)\n"
                + " FROM public.mail_attribute";
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                int countMail = rs.getInt(1);
                return countMail;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return 0;
    }

    public static int countUser() throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = " SELECT count(account.user_id) FROM user_info inner join account on account.user_id = user_info.user_id\n"
                + "inner join user_role on account.user_id = user_role.user_id\n"
                + "inner join role on user_role.role_id = role.role_id ";
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                int countUser = rs.getInt(1);
                return countUser;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return 0;
    }

    public static int countUserNew() throws SQLException {
        java.util.Date uDate = new java.util.Date();
        java.sql.Date sDate = convertUtilToSql(uDate);
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT count(account.user_id) FROM user_info inner join account on account.user_id = user_info.user_id\n"
                + "inner join user_role on account.user_id = user_role.user_id\n"
                + "inner join role on user_role.role_id = role.role_id \n"
                + "where account.date_create = ? ";
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            ps.setDate(1, sDate);
            rs = ps.executeQuery();
            if (rs.next()) {
                int countUser = rs.getInt(1);
                return countUser;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return 0;
    }

    public static int countRoleName(String role) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT count(role.role_name) FROM user_info inner join account on account.user_id = user_info.user_id\n"
                + "inner join user_role on account.user_id = user_role.user_id\n"
                + "inner join role on user_role.role_id = role.role_id\n"
                + "where role.role_name = ? ";
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            ps.setString(1, role);
            rs = ps.executeQuery();
            if (rs.next()) {
                int countRoleName = rs.getInt(1);
                return countRoleName;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return 0;
    }
}
