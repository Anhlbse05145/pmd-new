/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import com.dto.Get_Result_waitdto;
import com.dto.Push_Result_waitdto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bao Anh
 */

public class Api {

    private final static long LIMIT = 10;
    
    public static List<Get_Result_waitdto> getRwMail(Connection conn, long userId, long pageNumber) {
        long offset = (pageNumber - 1) * LIMIT;
        String sql = "SELECT * FROM result_wait WHERE result_wait.user_id = ? AND result_wait.flag IS NULL LIMIT ? OFFSET ?";
//        String sql = "SELECT * FROM result_wait WHERE result_wait.flag IS NULL LIMIT ? OFFSET ?";
        List<Get_Result_waitdto> result_waits = new ArrayList<>();
        try {
            PreparedStatement ps = conn.prepareCall(sql);
            ps.setLong(1, userId);
            ps.setLong(2, LIMIT);
            ps.setLong(3, offset);
            ResultSet rs = ps.executeQuery();
            Get_Result_waitdto result_wait = null;
            while (rs.next()) {
                long rw_id = rs.getLong("rw_id");
                String mailTitle = rs.getString("mail_title");
                result_wait = new Get_Result_waitdto(rw_id, mailTitle);
                result_waits.add(result_wait);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return result_waits;
    }
    
    public static Push_Result_waitdto updateRwMail(Connection conn, long rw_id, boolean flag) {
        String sql = "UPDATE result_wait SET flag = ? WHERE result_wait.rw_id = ?";
        Push_Result_waitdto rw = null;
        try{
            PreparedStatement ps = conn.prepareCall(sql);
            ps.setBoolean(1, true);
            ps.setLong(2, rw_id);
            int state = ps.executeUpdate();
            if(state != 0){
                rw = new Push_Result_waitdto(rw_id, flag);
            }
        } catch(SQLException ex){
            ex.printStackTrace();
        }
        return rw;
    }

}
