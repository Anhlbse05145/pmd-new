/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import java.util.regex.Pattern;

/**
 *
 * @author Mi
 */
public class CheckFrom {

    public static boolean isValidMail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
                + "[a-zA-Z0-9_+&*-]+)*@"
                + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
                + "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null) {
            return false;
        }
        return pat.matcher(email).matches();
    }

    public static boolean isValidPhone(String s) {
        String phoneRegex = "^(03|05|07|08|09)[0-9]{8}$";
        Pattern pat = Pattern.compile(phoneRegex);
        if (s == null) {
            return false;
        }
        return pat.matcher(s).matches();
    }

    public static boolean isValidSpecial(String input) {
        String specialChars = "~`!@#$%^&*()-_=+\\|[{]};:'\",<.>/?";
        char currentCharacter;
        boolean specialCharacterPresent = true;
        for (int i = 0; i < input.length(); i++) {
            currentCharacter = input.charAt(i);
            if (specialChars.contains(String.valueOf(currentCharacter))) {
                specialCharacterPresent = false;
            }
        }
        return specialCharacterPresent;
    }

    public static boolean isValidPassword(String password) {
        return password.length() >= 8;
    }

}
