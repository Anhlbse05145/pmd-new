/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import com.common.Const;
import com.entity.UserAccount;
import com.entity.UserInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.connect.DBContext;
import com.entity.SystemSettting;
import com.dto.PMAdto;
import com.dto.UpdatePMAdto;
import com.entity.AlgorithmParameter;
import com.entity.MailAccount;
import com.entity.Result_wait;
import com.entity.SystemSettingStr;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.security.SecureRandom;
import java.sql.Timestamp;

/**
 *
 * @author Mi
 */
public class DataLogin {

    private static SecureRandom random = new SecureRandom();

    private static final String ALPHA_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String ALPHA = "abcdefghijklmnopqrstuvwxyz";
    private static final String NUMERIC = "0123456789";
    private static final String SPECIAL_CHARS = "!@#$%^&*_=+-/";

    private final DBContext db;

    public DataLogin() throws Exception {
        db = new DBContext();
    }

    public static boolean checkActive(Connection conn, String userName, String password) throws SQLException, Exception {
        boolean active = false;
        String sql = "Select a.active from account a\n"
                + " where a.user_name = ? and a.user_password = ? ";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps = conn.prepareStatement(sql);
        ps.setString(1, userName);
        ps.setString(2, password);
        ResultSet rs = ps.executeQuery();
        try {
            if (rs.next()) {
                active = rs.getBoolean("active");
                return active;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return active;
    }

    public static UserAccount findUser(Connection conn, String userName, String password) throws SQLException, Exception {
        String sql = " Select a.user_id, a.user_name, a.user_password, a.date_create, a.date_update from account a "//
                + " where a.user_name = ? and a.user_password = ? ";
        PreparedStatement ps = conn.prepareStatement(sql);
        /*Nó được sử dụng để thực hiện truy vấn tham số.*/
        ps.setString(1, userName);
        /*set tham số*/
        ps.setString(2, password);
        ResultSet rs = ps.executeQuery(); //Thực thi câu lệnh SQL trả về đối tượng ResultSet.
        /*duy trì một con trỏ trỏ đến một hàng của một bảng. Ban đầu, con trỏ trỏ đến hàng đầu tiên.*/

        try {
            if (rs.next()) {
                int userId = rs.getInt("user_id");
                String dateCreate = rs.getString("date_create");
                String dateUpdate = rs.getString("date_update");

                UserAccount user = new UserAccount();
                user.setUserId(userId);
                user.setUserName(userName);
                user.setPassword(password);
                user.setDateCreate(dateCreate);
                user.setDateUpdate(dateUpdate);
                return user;
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return null;
    }

    // Điều kiện check cookie
    public static UserAccount findUser(Connection conn, String userName) throws SQLException, Exception {
        String sql = " Select a.user_name, a.user_password, a.date_create, a.date_update from account a "//
                + " where a.user_name = ? ";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, userName);
        ResultSet rs = ps.executeQuery();
        try {
            if (rs.next()) {
                String password = rs.getString("Password");
                String dateCreate = rs.getString("date_create");
                String dateUpdate = rs.getString("date_update");
                UserAccount user = new UserAccount();
                user.setUserName(userName);
                user.setPassword(password);
                user.setDateCreate(dateCreate);
                user.setDateUpdate(dateUpdate);
                return user;
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return null;
    }

    //Lấy ra info  user
    public static UserInfo infoUser(Connection conn, String userName) throws SQLException {
        String sql = "SELECT account.user_id, account.user_name, account.date_create, user_info.date_update, user_info.user_email, user_info.user_nation, \n"
                + "	user_info.user_mobile, role.role_name\n"
                + "	FROM user_info inner join account on account.user_id = user_info.user_id\n"
                + "	inner join user_role on account.user_id = user_role.user_id \n"
                + "	inner join role on user_role.role_id = role.role_id\n"
                + "	where account.user_name= ? ";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, userName);
        ResultSet rs = ps.executeQuery();
        UserInfo user = new UserInfo();
        try {
            if (rs.next()) {
                int userId = rs.getInt("user_id");
                String userMail = rs.getString("user_email");
                String userNation = rs.getString("user_nation");
                int userMobile = rs.getInt("user_mobile");
                String dateCreate = rs.getString("date_create");
                String dateUpdate = rs.getString("date_update");
                String role = rs.getString("role_name");
                user.setUserId(userId);
                user.setUserName(userName);
                user.setMail(userMail);
                user.setMobile(userMobile);
                user.setNation(userNation);
                user.setDateCreate(dateCreate);
                user.setDateUpdate(dateUpdate);
                user.setRole(role);
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return user;
    }
    //Lấy ra list info  user

    public static List<UserInfo> queryUserInfo(Connection conn) throws SQLException {
        String sql = "SELECT account.active, account.user_id, account.user_name, account.date_create, user_info.date_update, user_info.user_email, user_info.user_nation,\n"
                + "user_info.user_mobile, role.role_name\n"
                + "FROM user_info inner join account on account.user_id = user_info.user_id\n"
                + "inner join user_role on account.user_id = user_role.user_id\n"
                + "inner join role on user_role.role_id = role.role_id\n"
                + "ORDER BY account.user_name ASC ";
        PreparedStatement ps = conn.prepareStatement(sql);

        ResultSet rs = ps.executeQuery();
        List<UserInfo> list = new ArrayList<>();
        try {
            while (rs.next()) {
                boolean active = rs.getBoolean("active");
                int userId = rs.getInt("user_id");
                String userName = rs.getString("user_name");
                String userMail = rs.getString("user_email");
                String userNation = rs.getString("user_nation");
                int userMobile = rs.getInt("user_mobile");
                String dateCreate = rs.getString("date_create");
                String dateUpdate = rs.getString("date_update");
                String role = rs.getString("role_name");
                UserInfo info = new UserInfo();
                info.setActive(active);
                info.setUserId(userId);
                info.setUserName(userName);
                info.setMail(userMail);
                info.setNation(userNation);
                info.setMobile(userMobile);
                info.setDateCreate(dateCreate);
                info.setDateUpdate(dateUpdate);
                info.setRole(role);
                list.add(info);
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return list;
    }
    //Định dạng ngày tháng

    public static java.sql.Date convertUtilToSql(java.util.Date uDate) {
        java.sql.Date sDate = new java.sql.Date(uDate.getTime());
        return sDate;
    }

    //Edit info user
    public static int editInfo(Connection conn, String userId, String mobile, String nation) throws SQLException {
        String sql = " UPDATE public.user_info SET user_mobile=? ,user_nation=? , date_update=? WHERE user_id=? ";
        PreparedStatement ps = null;
        int phone = 0;
        int resultCow = 0;
        try {
            ps = conn.prepareStatement(sql);
            phone = Integer.parseInt(mobile);
            ps.setInt(1, phone);
            ps.setString(2, nation);
            ps.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            int result = Integer.parseInt(userId);
            ps.setInt(4, result);
            resultCow = ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }

        return resultCow;
    }
    // Reset Password user với khi dùng quyền Admin

    public static int resetPassword(Connection conn, String userId) throws SQLException {
        String sql = " UPDATE public.account SET user_password='12345678', date_update=? WHERE user_id=? ";
        PreparedStatement ps = conn.prepareStatement(sql);
        int result = 0;
        try {
            ps.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            int userIdInt = Integer.parseInt(userId);
            ps.setInt(2, userIdInt);
            result = ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }

        return result;
    }

    //Đổi Password
    public static String changePassword(Connection conn, String userName, String currentPassword, String newPassword) throws SQLException {
        String sqlCheck = " Select * from account a "//
                + " where a.user_name = ? And a.user_password = ? ";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(sqlCheck);
            ps.setString(1, userName);
            ps.setString(2, currentPassword);
            rs = ps.executeQuery();

            if (rs.next()) {
                String sql = " UPDATE public.account SET user_password=? , date_update=? WHERE user_name=? ";
                ps = conn.prepareStatement(sql);
                ps.setString(1, newPassword);
                ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
                ps.setString(3, userName);
                ps.executeUpdate();
                return Const.CHANGE_PASS_SUCCESS;
            } else {
                return Const.INVALID_CREDENTIALS;
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
    }

    // Đăng ký tài khoản
    public static void registerUser(String userName, String password) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        String sql = " INSERT INTO public.account "
                + " (user_password, user_name, date_create, date_update) "
                + " VALUES (?, ?, ?, ?) ";
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            ps.setString(1, password);
            ps.setString(2, userName);
            ps.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            ps.executeUpdate();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
    }

    //check UserName tồn tại
    public static int checkName(String userName) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sqlCheck = " Select a.user_id from account a "
                + " where a.user_name = ? ";
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sqlCheck);
            ps.setString(1, userName);
            rs = ps.executeQuery();
            if (rs.next()) {
                int userId = rs.getInt("user_id");
                return userId;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return 0;
    }

    //Lấy ID user
    public static int getID(String userName) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sqlCheck = " Select a.user_id from account a "
                + " where a.user_name = ? ";
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sqlCheck);
            ps.setString(1, userName);
            rs = ps.executeQuery();
            if (rs.next()) {
                int userId = rs.getInt("user_id");
                return userId;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return 0;
    }

    // Lưu thông tin tài khoản đăng ký
    public static void registerInfoUser(String userName, String mail, String mobile, String nation) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        java.util.Date uDate = new java.util.Date();
        java.sql.Date sDate = convertUtilToSql(uDate);
        int id = getID(userName);
        String sql = " INSERT INTO public.user_info "
                + " (user_id, user_email, user_nation, user_mobile, date_update) "
                + " VALUES ( ?, ?, ?, ?, ?); ";
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setString(2, mail);
            ps.setString(3, nation);
            int phone = Integer.parseInt(mobile);
            ps.setInt(4, phone);
            ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
            ps.executeUpdate();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
    }

    // Lấy role của user
    public static String roleUser(String userName) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT role.role_name "
                + "FROM user_info inner join account on account.user_id = user_info.user_id\n"
                + "inner join user_role on account.user_id = user_role.user_id\n"
                + "inner join role on user_role.role_id = role.role_id\n"
                + "where account.user_name=? ";
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            ps.setString(1, userName);
            rs = ps.executeQuery();
            if (rs.next()) {
                String roleName = rs.getString("role_name");
                return roleName;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return null;
    }

    //Đăng ký role id
    public static void registerRoleId(String userName) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        int id = getID(userName);
        String sql = "INSERT INTO public.user_role(user_id, date_create, date_update)\n"
                + " VALUES ( ?, ?, ?)";
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            ps.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            ps.executeUpdate();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
    }

    //Gọi role id
    public static int getRoleID(String userName) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id = getID(userName);
        String sqlCheck = "SELECT role_id"
                + " FROM public.user_role where user_id = ? ";
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sqlCheck);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                int roleId = rs.getInt("role_id");
                return roleId;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return 0;
    }

    //Tạo role khởi tạo của user
    public static void registerRoleName(String userName) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        int id = getRoleID(userName);
        String roleName = "user";
        String sql = "INSERT INTO public.role("
                + " date_create, date_update, role_name, role_id)"
                + " VALUES ( ?, ?, ?, ?)";
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            ps.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            ps.setString(3, roleName);
            ps.setInt(4, id);
            ps.executeUpdate();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
    }

    //Hàm random
    public static String generatePassword(String dic) {
        String result = "";
        for (int i = 0; i < 8; i++) {
            int index = random.nextInt(dic.length());
            result += dic.charAt(index);
        }
        return result;
    }

    //Xác thực tài khoản
    public static int checkInfo(String userName, String mail, int mobile) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sqlCheck = " SELECT account.user_id FROM account inner join user_info on account.user_id = user_info.user_id "
                + "where account.user_name =? and user_info.user_email =? and  user_info.user_mobile =? ";
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sqlCheck);
            ps.setString(1, userName);
            ps.setString(2, mail);
            ps.setInt(3, mobile);
            rs = ps.executeQuery();
            if (rs.next()) {
                int userId = rs.getInt("user_id");
                return userId;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return 0;
    }

    //Khôi phục mật khẩu
    public static String forgetPassword(String userName) throws SQLException, ClassNotFoundException {
        Connection conn = null;
        PreparedStatement ps = null;
        int result = 0;
        String password = "";
        try {
            conn = DBContext.getPostgres();
            password = generatePassword(ALPHA_CAPS + ALPHA);
            String sql = " UPDATE public.account SET user_password=? , date_update=? WHERE user_name=? ";
            ps = conn.prepareStatement(sql);
            ps.setString(1, password);
            ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            ps.setString(3, userName);
            result = ps.executeUpdate();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        if (result > 0) {
            return password;
        }
        return "";
    }

    // Chỉnh sửa trạng thái active
    public static int editActive(String ID, String active) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        int id = Integer.parseInt(ID);
        String sql = " UPDATE public.account SET active=?, date_update=? WHERE user_id=? ";
        boolean check;
        int row = 0;
        if ("true".equals(active)) {
            check = true;
        } else {
            check = false;
        }
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            ps.setBoolean(1, check);
            ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            ps.setInt(3, id);
            row = ps.executeUpdate();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return row;
    }

    public static int editActivePMA(String ID, String active) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        long id = Long.parseLong(ID);
        boolean activeFlag = Boolean.parseBoolean(active);
        String sql = " UPDATE public.personal_mail_account SET active=?, date_update=? WHERE pma_id=? ";
        int row = 0;
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            ps.setBoolean(1, activeFlag);
            ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            ps.setLong(3, id);
            row = ps.executeUpdate();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return row;
    }

    public static MailAccount getPMAfromID(Connection conn, String id) throws Exception {
        MailAccount mailAccount = new MailAccount();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM public.personal_mail_account WHERE pma_id = ?";
        long pmaId = Long.parseLong(id);
        try {
            ps = conn.prepareStatement(sql);
            ps.setLong(1, pmaId);
            rs = ps.executeQuery();
            while (rs.next()) {
                long pmaID = rs.getLong("pma_id");
                String mailAcc = rs.getString("user_name");
                long isId = rs.getLong("is_id");
                boolean active = rs.getBoolean("active");
                long userId = rs.getLong("user_id");
                String password = rs.getString("password");
                mailAccount.setPmaID(pmaID);
                mailAccount.setMailAccount(mailAcc);
                mailAccount.setIsId(isId);
                mailAccount.setActive(active);
                mailAccount.setUserId(userId);
                mailAccount.setPassword(password);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return mailAccount;
    }

    // Chỉnh sửa Role của user
    public static int editRole(String userName, String role) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        int id = getRoleID(userName);
        String sql = " UPDATE public.role SET role_name=?, date_update=? WHERE role_id=? ";

        System.out.println("com.model.DataLogin.editRole(): " + role + " id: " + id);
        int result = 0;
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            ps.setString(1, role);
            ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            ps.setInt(3, id);
            result = ps.executeUpdate();

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return result;
    }

    // Kiểm tra xem tài khoản đã thêm Personal mail account hay chưa
    public static int checkPersonalMailAccount(String userName) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT pma_id\n"
                + " FROM public.personal_mail_account where user_id = ? ";
        int id = getID(userName);
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                int pmaId = rs.getInt("pma_id");
                return pmaId;

            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return 0;
    }

    // Chỉnh sửa thông Mail Account tin cần đăng ký
    public static void editPersonalMailAccount(String pmaID, String mailAccount, String password, String mailServer, String active) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        int id = Integer.parseInt(pmaID);
        boolean check;
        if ("true".equals(active)) {
            check = true;
        } else {
            check = false;
        }
        String sql = " UPDATE public.personal_mail_account\n"
                + " SET date_update=?, user_name=?, password=?, mail_server=?, active=?\n"
                + " WHERE pma_id=? ";
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            ps.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            ps.setString(2, mailAccount);
            ps.setString(3, password);
            ps.setString(4, mailServer);
            ps.setBoolean(5, check);
            ps.setInt(6, id);
            ps.executeUpdate();

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
    }

    // Đăng ký thông tin Mail Account
    public static int cratePersonalMailAccount(String userName, String mailAccount, String password, String isIdInput, String activeInput) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        int result = 0;
        int id = getID(userName);
        String sql = "INSERT INTO public.personal_mail_account(\n"
                + " date_create, date_update, user_name, password, is_id, active, user_id)\n"
                + " VALUES ( ?, ?, ?, ?, ?, ?, ? )";
        try {
            boolean active = Boolean.parseBoolean(activeInput);
            long isId = Long.parseLong(isIdInput);
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            ps.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            ps.setString(3, mailAccount);
            ps.setString(4, password);
            ps.setLong(5, isId);
            ps.setBoolean(6, active);
            ps.setInt(7, id);
            result = ps.executeUpdate();

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return result;
    }

    public static List<MailAccount> getMailAccount(Connection conn, String userName) throws SQLException {
        List<MailAccount> mailAcc = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id = getID(userName);
        String sql = "SELECT pma_id, date_create, date_update, user_name, password, is_id, active, is_verify"
                + " FROM public.personal_mail_account where user_id = ? ";
        try {
            conn = DBContext.getPostgres();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                int pma_id = rs.getInt("pma_id");
                String date_create = rs.getString("date_create");
                String date_update = rs.getString("date_update");
                String user_name = rs.getString("user_name");
                String password = rs.getString("password");
                long isId = rs.getLong("is_id");
                boolean active = rs.getBoolean("active");
                boolean isVerify = rs.getBoolean("is_verify");
                mailAcc.add(new MailAccount(pma_id, date_create, date_update, user_name, password, isId, active, id, isVerify));

            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataLogin.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return mailAcc;
    }

    public static PMAdto getPMA(Connection conn, long rw_id) throws Exception {
        PMAdto pMAdto = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT pma_id, date_create, date_update, user_name, password, mail_server, active, user_id "
                + "FROM public.personal_mail_account WHERE pma_id = (SELECT pma_id FROM result_wait WHERE rw_id = ?)";
        try {
            ps = conn.prepareStatement(sql);
            ps.setLong(1, rw_id);
            rs = ps.executeQuery();
            while (rs.next()) {
                String user_name = rs.getString("user_name");
                String password = rs.getString("password");
                String mail_server = rs.getString("mail_server");
                pMAdto = new PMAdto(user_name, password, mail_server);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }

        return pMAdto;
    }

    public static Result_wait findRw(Connection conn, long rw_id) throws Exception {
        Result_wait result_wait = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM result_wait WHERE rw_id = ?";
        try {
            ps = conn.prepareStatement(sql);
            ps.setLong(1, rw_id);
            rs = ps.executeQuery();
            while (rs.next()) {
                long mail_uid = rs.getLong("mail_uid");
                boolean flag = rs.getBoolean("flag");
                result_wait = new Result_wait();
                result_wait.setMail_Uid(mail_uid);
                result_wait.setFlag(flag);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }

        return result_wait;
    }

    public static String getEmailfromUser(Connection conn, String userName) throws Exception {
        String mail = "";
        String sql = "SELECT user_email FROM public.user_info "
                + "WHERE user_id = (SELECT user_id from account WHERE user_name = ?)";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, userName);
            rs = ps.executeQuery();
            while (rs.next()) {
                mail = rs.getString(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }

        return mail;
    }

    public static SystemSettting getSystemSetting(Connection conn) throws Exception {
        SystemSettting systemSetting = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT setting FROM public.pmd_setting";
        Gson gson = null;
        String setting = "";
        try {
            gson = new Gson();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                setting = rs.getString(1);
                setting = "{" + setting + "}";
            }
            SystemSettingStr sss = gson.fromJson(setting, SystemSettingStr.class);
            systemSetting = new SystemSettting(sss.getFetch_time(), sss.getTest_data_size(), sss.getRetraining_time());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }

        return systemSetting;
    }

    public static int updateSystemSetting(Connection conn, String json) throws Exception {
        Gson gson = new Gson();
        int result = 0;
        PreparedStatement ps = null;
        String sql = "UPDATE public.pmd_setting SET setting = setting || ? :: hstore";
        try {
            ps = conn.prepareStatement(sql);
            ps.setObject(1, json);
            result = ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }

        return result;
    }

    public static int UpdatePMAInfo(Connection conn, UpdatePMAdto mAdto) throws Exception {
        PreparedStatement ps = null;
        String sql = "UPDATE public.personal_mail_account SET date_update=?, user_name=?, is_id=?, active=? WHERE pma_id = ?";
        int result = 0;
        try {
            ps = conn.prepareStatement(sql);
            ps.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            ps.setString(2, mAdto.getUserNAme());
            ps.setLong(3, Long.parseLong(mAdto.getIsId()));
            ps.setBoolean(4, Boolean.parseBoolean(mAdto.getActive()));
            ps.setLong(5, Long.parseLong(mAdto.getPmaId()));
            result = ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return result;
    }

    public static AlgorithmParameter getDataChart(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String data = "";
        String sql = "SELECT attr FROM public.algorithm_parameter";
        AlgorithmParameter ap = null;
        Gson gson = null;
        try {
            gson = new Gson();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                data = rs.getString("attr");
                data = "{" + data + "}";
            }
            ap = gson.fromJson(data, AlgorithmParameter.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (rs != null && rs.isClosed()) {
                rs.close();
            }
            if (ps != null && ps.isClosed()) {
                ps.close();
            }
            if (conn != null && conn.isClosed()) {
                conn.close();
            }
        }
        return ap;
    }
}
