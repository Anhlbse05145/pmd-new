/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

import com.entity.UserInfo;
import java.util.List;

/**
 *
 * @author Bao Anh
 */
public class UserInfodto {
    
    List<UserInfo> userInfos;
    String errorString;

    public UserInfodto(List<UserInfo> userInfos, String errorString) {
        this.userInfos = userInfos;
        this.errorString = errorString;
    }

    public List<UserInfo> getUserInfos() {
        return userInfos;
    }

    public void setUserInfos(List<UserInfo> userInfos) {
        this.userInfos = userInfos;
    }

    public String getErrorString() {
        return errorString;
    }

    public void setErrorString(String errorString) {
        this.errorString = errorString;
    }
    
}
