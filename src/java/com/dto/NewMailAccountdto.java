/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

/**
 *
 * @author Bao Anh
 */
public class NewMailAccountdto {
    
    private String mail;
    private String mailPass;
    private String accPass;
    private String mailIsId;
    private String active;

    public NewMailAccountdto(String mail, String mailPass, String accPass, String mailIsId, String active) {
        this.mail = mail;
        this.mailPass = mailPass;
        this.accPass = accPass;
        this.mailIsId = mailIsId;
        this.active = active;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMailPass() {
        return mailPass;
    }

    public void setMailPass(String mailPass) {
        this.mailPass = mailPass;
    }

    public String getAccPass() {
        return accPass;
    }

    public void setAccPass(String accPass) {
        this.accPass = accPass;
    }

    public String getMailIsId() {
        return mailIsId;
    }

    public void setMailIsId(String mailIsId) {
        this.mailIsId = mailIsId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
    
    
    
}
