/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

/**
 *
 * @author Bao Anh
 */
public class Userdto {

    private String userId;
    private String nation;
    private String mobile;
    private String confirmPass;

    public Userdto() {
    }

    public Userdto(String userId, String nation, String mobile, String confirmPass) {
        this.userId = userId;
        this.nation = nation;
        this.mobile = mobile;
        this.confirmPass = confirmPass;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getConfirmPass() {
        return confirmPass;
    }

    public void setConfirmPass(String confirmPass) {
        this.confirmPass = confirmPass;
    }

}
