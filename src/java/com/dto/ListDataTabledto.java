/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

import java.util.List;

/**
 *
 * @author Bao Anh
 */
public class ListDataTabledto {
    private List<DataTabledto> liDataTable;
    private String errorString;

    public ListDataTabledto(List<DataTabledto> liDataTable, String errorString) {
        this.liDataTable = liDataTable;
        this.errorString = errorString;
    }

    public List<DataTabledto> getLiDataTable() {
        return liDataTable;
    }

    public void setLiDataTable(List<DataTabledto> liDataTable) {
        this.liDataTable = liDataTable;
    }

    public String getErrorString() {
        return errorString;
    }

    public void setErrorString(String errorString) {
        this.errorString = errorString;
    }
    
    
}
