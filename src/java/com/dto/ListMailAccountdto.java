/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

import com.entity.MailAccount;
import java.util.List;

/**
 *
 * @author Bao Anh
 */
public class ListMailAccountdto {
    
    private List<MailAccount> listMailAcc;
    private String errorString;

    public ListMailAccountdto(List<MailAccount> listMailAcc, String errorString) {
        this.listMailAcc = listMailAcc;
        this.errorString = errorString;
    }

    public List<MailAccount> getListMailAcc() {
        return listMailAcc;
    }

    public void setListMailAcc(List<MailAccount> listMailAcc) {
        this.listMailAcc = listMailAcc;
    }

    public String getErrorString() {
        return errorString;
    }

    public void setErrorString(String errorString) {
        this.errorString = errorString;
    }
    
}
