/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

/**
 *
 * @author Bao Anh
 */
public class UpdatePMAdto {
    String pmaId;
    String userNAme;
    String isId;
    String active;

    public UpdatePMAdto(String pmaId, String userNAme, String isId, String active) {
        this.pmaId = pmaId;
        this.userNAme = userNAme;
        this.isId = isId;
        this.active = active;
    }

    public String getPmaId() {
        return pmaId;
    }

    public void setPmaId(String pmaId) {
        this.pmaId = pmaId;
    }
 
    public String getUserNAme() {
        return userNAme;
    }

    public void setUserNAme(String userNAme) {
        this.userNAme = userNAme;
    }

    public String getIsId() {
        return isId;
    }

    public void setIsId(String mailServer) {
        this.isId = isId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
    
}
