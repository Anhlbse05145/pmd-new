/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

/**
 *
 * @author Bao Anh
 */
public class PMAdto {
    
    private long pmaId;
    private String userName;
    private String password;
    private String mailServer;

    public PMAdto(String userName, String password, String mailServer) {
        this.userName = userName;
        this.password = password;
        this.mailServer = mailServer;
    }

    public long getPmaId() {
        return pmaId;
    }

    public void setPmaId(long pmaId) {
        this.pmaId = pmaId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMailServer() {
        return mailServer;
    }

    public void setMailServer(String mailServer) {
        this.mailServer = mailServer;
    }
    
}
