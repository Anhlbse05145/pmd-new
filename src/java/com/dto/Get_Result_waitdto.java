/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

/**
 *
 * @author Bao Anh
 */
public class Get_Result_waitdto {
    long rw_id;
    String mailTitle;

    public Get_Result_waitdto(long rw_id, String mailTitle) {
        this.rw_id = rw_id;
        this.mailTitle = mailTitle;
    }

    public long getRw_id() {
        return rw_id;
    }

    public void setRw_id(long rw_id) {
        this.rw_id = rw_id;
    }

    public String getMailTitle() {
        return mailTitle;
    }

    public void setMailTitle(String mailTitle) {
        this.mailTitle = mailTitle;
    }
    
}
