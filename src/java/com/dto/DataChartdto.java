/*
 * To change this license header; choose License Headers in Project Properties.
 * To change this template file; choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

/**
 *
 * @author Bao Anh
 */
public class DataChartdto {
    
    private double body_list_word;
    private double body_html;
    private double number_of_linked_to_sender_domain;
    private double sender_domain_not_match_body_domain;
    private double presence_form_tag;
    private double url_based_image_source;
    private double body_no_characters;
    private double body_no_distinct_words;
    private double body_no_function_words;
    private double body_no_words;
    private double body_richness;
    private double sender_is_subdomain;
    private double send_no_characters;
    private double send_no_words;
    private double send_diff_sender_reply_to;
    private double send_non_modal_sender_domain;
    private double subj_list_word;
    private double subj_reply;
    private double subj_no_words;
    private double subj_no_characters;
    private double url_at_symbol;
    private double url_ip_address;
    private double url_link_text;
    private double url_no_domains;
    private double url_max_no_periods;
    private double url_no_ext_links;
    private double url_no_links;
    private double url_no_int_links;
    private double url_no_ip_addresses;
    private double url_no_img_links;
    private double url_no_ports;
    private double url_non_modal_links;
    private double url_ports;
    private double is_multipart;
    private double have_attachment;

    public DataChartdto() {
    }

    public DataChartdto(double body_list_word, double body_html, double number_of_linked_to_sender_domain, double sender_domain_not_match_body_domain, double presence_form_tag, double url_based_image_source, double body_no_characters, double body_no_distinct_words, double body_no_function_words, double body_no_words, double body_richness, double sender_is_subdomain, double send_no_characters, double send_no_words, double send_diff_sender_reply_to, double send_non_modal_sender_domain, double subj_list_word, double subj_reply, double subj_no_words, double subj_no_characters, double url_at_symbol, double url_ip_address, double url_link_text, double url_no_domains, double url_max_no_periods, double url_no_ext_links, double url_no_links, double url_no_int_links, double url_no_ip_addresses, double url_no_img_links, double url_no_ports, double url_non_modal_links, double url_ports, double is_multipart, double have_attachment) {
        this.body_list_word = body_list_word;
        this.body_html = body_html;
        this.number_of_linked_to_sender_domain = number_of_linked_to_sender_domain;
        this.sender_domain_not_match_body_domain = sender_domain_not_match_body_domain;
        this.presence_form_tag = presence_form_tag;
        this.url_based_image_source = url_based_image_source;
        this.body_no_characters = body_no_characters;
        this.body_no_distinct_words = body_no_distinct_words;
        this.body_no_function_words = body_no_function_words;
        this.body_no_words = body_no_words;
        this.body_richness = body_richness;
        this.sender_is_subdomain = sender_is_subdomain;
        this.send_no_characters = send_no_characters;
        this.send_no_words = send_no_words;
        this.send_diff_sender_reply_to = send_diff_sender_reply_to;
        this.send_non_modal_sender_domain = send_non_modal_sender_domain;
        this.subj_list_word = subj_list_word;
        this.subj_reply = subj_reply;
        this.subj_no_words = subj_no_words;
        this.subj_no_characters = subj_no_characters;
        this.url_at_symbol = url_at_symbol;
        this.url_ip_address = url_ip_address;
        this.url_link_text = url_link_text;
        this.url_no_domains = url_no_domains;
        this.url_max_no_periods = url_max_no_periods;
        this.url_no_ext_links = url_no_ext_links;
        this.url_no_links = url_no_links;
        this.url_no_int_links = url_no_int_links;
        this.url_no_ip_addresses = url_no_ip_addresses;
        this.url_no_img_links = url_no_img_links;
        this.url_no_ports = url_no_ports;
        this.url_non_modal_links = url_non_modal_links;
        this.url_ports = url_ports;
        this.is_multipart = is_multipart;
        this.have_attachment = have_attachment;
    }

    public double getBody_list_word() {
        return body_list_word;
    }

    public void setBody_list_word(double body_list_word) {
        this.body_list_word = body_list_word;
    }

    public double getBody_html() {
        return body_html;
    }

    public void setBody_html(double body_html) {
        this.body_html = body_html;
    }

    public double getNumber_of_linked_to_sender_domain() {
        return number_of_linked_to_sender_domain;
    }

    public void setNumber_of_linked_to_sender_domain(double number_of_linked_to_sender_domain) {
        this.number_of_linked_to_sender_domain = number_of_linked_to_sender_domain;
    }

    public double getSender_domain_not_match_body_domain() {
        return sender_domain_not_match_body_domain;
    }

    public void setSender_domain_not_match_body_domain(double sender_domain_not_match_body_domain) {
        this.sender_domain_not_match_body_domain = sender_domain_not_match_body_domain;
    }

    public double getPresence_form_tag() {
        return presence_form_tag;
    }

    public void setPresence_form_tag(double presence_form_tag) {
        this.presence_form_tag = presence_form_tag;
    }

    public double getUrl_based_image_source() {
        return url_based_image_source;
    }

    public void setUrl_based_image_source(double url_based_image_source) {
        this.url_based_image_source = url_based_image_source;
    }

    public double getBody_no_characters() {
        return body_no_characters;
    }

    public void setBody_no_characters(double body_no_characters) {
        this.body_no_characters = body_no_characters;
    }

    public double getBody_no_distinct_words() {
        return body_no_distinct_words;
    }

    public void setBody_no_distinct_words(double body_no_distinct_words) {
        this.body_no_distinct_words = body_no_distinct_words;
    }

    public double getBody_no_function_words() {
        return body_no_function_words;
    }

    public void setBody_no_function_words(double body_no_function_words) {
        this.body_no_function_words = body_no_function_words;
    }

    public double getBody_no_words() {
        return body_no_words;
    }

    public void setBody_no_words(double body_no_words) {
        this.body_no_words = body_no_words;
    }

    public double getBody_richness() {
        return body_richness;
    }

    public void setBody_richness(double body_richness) {
        this.body_richness = body_richness;
    }

    public double getSender_is_subdomain() {
        return sender_is_subdomain;
    }

    public void setSender_is_subdomain(double sender_is_subdomain) {
        this.sender_is_subdomain = sender_is_subdomain;
    }

    public double getSend_no_characters() {
        return send_no_characters;
    }

    public void setSend_no_characters(double send_no_characters) {
        this.send_no_characters = send_no_characters;
    }

    public double getSend_no_words() {
        return send_no_words;
    }

    public void setSend_no_words(double send_no_words) {
        this.send_no_words = send_no_words;
    }

    public double getSend_diff_sender_reply_to() {
        return send_diff_sender_reply_to;
    }

    public void setSend_diff_sender_reply_to(double send_diff_sender_reply_to) {
        this.send_diff_sender_reply_to = send_diff_sender_reply_to;
    }

    public double getSend_non_modal_sender_domain() {
        return send_non_modal_sender_domain;
    }

    public void setSend_non_modal_sender_domain(double send_non_modal_sender_domain) {
        this.send_non_modal_sender_domain = send_non_modal_sender_domain;
    }

    public double getSubj_list_word() {
        return subj_list_word;
    }

    public void setSubj_list_word(double subj_list_word) {
        this.subj_list_word = subj_list_word;
    }

    public double getSubj_reply() {
        return subj_reply;
    }

    public void setSubj_reply(double subj_reply) {
        this.subj_reply = subj_reply;
    }

    public double getSubj_no_words() {
        return subj_no_words;
    }

    public void setSubj_no_words(double subj_no_words) {
        this.subj_no_words = subj_no_words;
    }

    public double getSubj_no_characters() {
        return subj_no_characters;
    }

    public void setSubj_no_characters(double subj_no_characters) {
        this.subj_no_characters = subj_no_characters;
    }

    public double getUrl_at_symbol() {
        return url_at_symbol;
    }

    public void setUrl_at_symbol(double url_at_symbol) {
        this.url_at_symbol = url_at_symbol;
    }

    public double getUrl_ip_address() {
        return url_ip_address;
    }

    public void setUrl_ip_address(double url_ip_address) {
        this.url_ip_address = url_ip_address;
    }

    public double getUrl_link_text() {
        return url_link_text;
    }

    public void setUrl_link_text(double url_link_text) {
        this.url_link_text = url_link_text;
    }

    public double getUrl_no_domains() {
        return url_no_domains;
    }

    public void setUrl_no_domains(double url_no_domains) {
        this.url_no_domains = url_no_domains;
    }

    public double getUrl_max_no_periods() {
        return url_max_no_periods;
    }

    public void setUrl_max_no_periods(double url_max_no_periods) {
        this.url_max_no_periods = url_max_no_periods;
    }

    public double getUrl_no_ext_links() {
        return url_no_ext_links;
    }

    public void setUrl_no_ext_links(double url_no_ext_links) {
        this.url_no_ext_links = url_no_ext_links;
    }

    public double getUrl_no_links() {
        return url_no_links;
    }

    public void setUrl_no_links(double url_no_links) {
        this.url_no_links = url_no_links;
    }

    public double getUrl_no_int_links() {
        return url_no_int_links;
    }

    public void setUrl_no_int_links(double url_no_int_links) {
        this.url_no_int_links = url_no_int_links;
    }

    public double getUrl_no_ip_addresses() {
        return url_no_ip_addresses;
    }

    public void setUrl_no_ip_addresses(double url_no_ip_addresses) {
        this.url_no_ip_addresses = url_no_ip_addresses;
    }

    public double getUrl_no_img_links() {
        return url_no_img_links;
    }

    public void setUrl_no_img_links(double url_no_img_links) {
        this.url_no_img_links = url_no_img_links;
    }

    public double getUrl_no_ports() {
        return url_no_ports;
    }

    public void setUrl_no_ports(double url_no_ports) {
        this.url_no_ports = url_no_ports;
    }

    public double getUrl_non_modal_links() {
        return url_non_modal_links;
    }

    public void setUrl_non_modal_links(double url_non_modal_links) {
        this.url_non_modal_links = url_non_modal_links;
    }

    public double getUrl_ports() {
        return url_ports;
    }

    public void setUrl_ports(double url_ports) {
        this.url_ports = url_ports;
    }

    public double getIs_multipart() {
        return is_multipart;
    }

    public void setIs_multipart(double is_multipart) {
        this.is_multipart = is_multipart;
    }

    public double getHave_attachment() {
        return have_attachment;
    }

    public void setHave_attachment(double have_attachment) {
        this.have_attachment = have_attachment;
    }
    
    
    
}
