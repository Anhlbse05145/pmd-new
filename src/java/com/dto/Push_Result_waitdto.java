/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

/**
 *
 * @author Bao Anh
 */
public class Push_Result_waitdto {
    long rw_id;
    boolean flag;

    public Push_Result_waitdto(long rw_id, boolean flag) {
        this.rw_id = rw_id;
        this.flag = flag;
    }

    public long getRw_id() {
        return rw_id;
    }

    public void setRw_id(long rw_id) {
        this.rw_id = rw_id;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
    
}
