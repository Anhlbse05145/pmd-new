/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

/**
 *
 * @author Bao Anh
 */
public class ChangePassdto {
    
    private String currentPassword;
    private String newPassword;
    private String confirmPassword;

    public ChangePassdto(String currentPasswordTxt, String newPasswordTxt, String confirmPasswordTxt) {
        this.currentPassword = currentPasswordTxt;
        this.newPassword = newPasswordTxt;
        this.confirmPassword = confirmPasswordTxt;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPasswordTxt) {
        this.currentPassword = currentPasswordTxt;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPasswordTxt) {
        this.newPassword = newPasswordTxt;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPasswordTxt) {
        this.confirmPassword = confirmPasswordTxt;
    }
    
}
