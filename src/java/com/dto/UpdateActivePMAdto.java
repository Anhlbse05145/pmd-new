/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

/**
 *
 * @author Bao Anh
 */
public class UpdateActivePMAdto {
    String pmaID;
    String active;

    public UpdateActivePMAdto(String pmaID, String active) {
        this.pmaID = pmaID;
        this.active = active;
    }

    public String getPmaId() {
        return pmaID;
    }

    public void setPmaId(String pmaID) {
        this.pmaID = pmaID;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
    
}
