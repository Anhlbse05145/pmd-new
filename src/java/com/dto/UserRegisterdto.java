/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

/**
 *
 * @author Bao Anh
 */
public class UserRegisterdto {

    private String userName;
    private String password;
    private String confirmPassword;
    private String mail;
    private String mobile;
    private String nation;

    public UserRegisterdto() {
    }

    public UserRegisterdto(String userName, String newPassword, String confirmPassword, String mail, String mobile, String nation) {
        this.userName = userName;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.mail = mail;
        this.mobile = mobile;
        this.nation = nation;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String newPassword) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }
}
