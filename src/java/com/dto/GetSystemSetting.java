/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

import com.entity.SystemSettting;

/**
 *
 * @author Bao Anh
 */
public class GetSystemSetting {
    
    private SystemSettting systemSettting;
    private String errorString;

    public GetSystemSetting(SystemSettting systemSettting, String errorString) {
        this.systemSettting = systemSettting;
        this.errorString = errorString;
    }

    public SystemSettting getSystemSettting() {
        return systemSettting;
    }

    public void setSystemSettting(SystemSettting systemSettting) {
        this.systemSettting = systemSettting;
    }

    public String getErrorString() {
        return errorString;
    }

    public void setErrorString(String errorString) {
        this.errorString = errorString;
    }
    
}
