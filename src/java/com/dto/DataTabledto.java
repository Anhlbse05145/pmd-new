/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

/**
 *
 * @author Bao Anh
 */
public class DataTabledto {

    private double TruePositive;
    private double TrueNegative;
    private double FalseNegative;
    private double FalsePositive;
    private double FalsePositiveRate;
    private double FalseNegativeRate;
    private double TruePositiveRate;
    private double TrueNegativeRate;
    private double Time;
    private double Accuracy;

    public DataTabledto() {
    }

    public DataTabledto(double TruePositive, double TrueNegative, double FalseNegative, double FalsePositive, double FalsePositiveRate, double FalseNegativeRate, double TruePositiveRate, double TrueNegativeRate, double Time, double Accuracy) {
        this.TruePositive = TruePositive;
        this.TrueNegative = TrueNegative;
        this.FalseNegative = FalseNegative;
        this.FalsePositive = FalsePositive;
        this.FalsePositiveRate = FalsePositiveRate;
        this.FalseNegativeRate = FalseNegativeRate;
        this.TruePositiveRate = TruePositiveRate;
        this.TrueNegativeRate = TrueNegativeRate;
        this.Time = Time;
        this.Accuracy = Accuracy;
    }

    public double getTruePositive() {
        return TruePositive;
    }

    public void setTruePositive(double TruePositive) {
        this.TruePositive = TruePositive;
    }

    public double getTrueNegative() {
        return TrueNegative;
    }

    public void setTrueNegative(double TrueNegative) {
        this.TrueNegative = TrueNegative;
    }

    public double getFalseNegative() {
        return FalseNegative;
    }

    public void setFalseNegative(double FalseNegative) {
        this.FalseNegative = FalseNegative;
    }

    public double getFalsePositive() {
        return FalsePositive;
    }

    public void setFalsePositive(double FalsePositive) {
        this.FalsePositive = FalsePositive;
    }

    public double getFalsePositiveRate() {
        return FalsePositiveRate;
    }

    public void setFalsePositiveRate(double FalsePositiveRate) {
        this.FalsePositiveRate = FalsePositiveRate;
    }

    public double getFalseNegativeRate() {
        return FalseNegativeRate;
    }

    public void setFalseNegativeRate(double FalseNegativeRate) {
        this.FalseNegativeRate = FalseNegativeRate;
    }

    public double getTruePositiveRate() {
        return TruePositiveRate;
    }

    public void setTruePositiveRate(double TruePositiveRate) {
        this.TruePositiveRate = TruePositiveRate;
    }

    public double getTrueNegativeRate() {
        return TrueNegativeRate;
    }

    public void setTrueNegativeRate(double TrueNegativeRate) {
        this.TrueNegativeRate = TrueNegativeRate;
    }

    public double getTime() {
        return Time;
    }

    public void setTime(double Time) {
        this.Time = Time;
    }

    public double getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(double Accuracy) {
        this.Accuracy = Accuracy;
    }
    
    

}
