/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto;

/**
 *
 * @author Bao Anh
 */
public class UpdateSystemSettingdto {
    private String retraining_time;
    private String test_data_size;
    private String fetch_time;

    public UpdateSystemSettingdto(String retraining_time, String test_data_size, String fetch_time) {
        this.retraining_time = retraining_time;
        this.test_data_size = test_data_size;
        this.fetch_time = fetch_time;
    }

    public String getRetraining_time() {
        return retraining_time;
    }

    public void setRetraining_time(String reTrainTime) {
        this.retraining_time = reTrainTime;
    }

    public String getTest_data_size() {
        return test_data_size;
    }

    public void setTest_data_size(String test_data_size) {
        this.test_data_size = test_data_size;
    }

    public String getFetch_time() {
        return fetch_time;
    }

    public void setFetch_time(String fetch_time) {
        this.fetch_time = fetch_time;
    }
    
}
