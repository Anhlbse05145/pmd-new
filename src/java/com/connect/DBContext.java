/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Mi
 */
public class DBContext {

    public static Connection getPostgres()
            throws ClassNotFoundException, SQLException {
        // Chú ý: Thay đổi các thông số kết nối cho phù hợp.
        String hostName = "150.95.104.39";
        String dbName = "pmdsystem";
        String userName = "knistro";
        String password = "Hoalac@123";
        return getPostgres(hostName, dbName, userName, password);
    }

    private static Connection getPostgres(String hostName, String dbName, String userName, String password) throws SQLException,
            ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        String connectionURL = "jdbc:postgresql://" + hostName + ":5432/" + dbName;
        Connection conn = DriverManager.getConnection(connectionURL, userName, password);
        return conn;
    }

}
