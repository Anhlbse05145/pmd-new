
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>System Setting</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="User Information">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="images/favicon.png">
        <link rel="shortcut icon" href="images/favicon.png">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
        <link rel="stylesheet" href="assets/css/lib/datatable/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    </head>
</head>
<body>


    <jsp:include page="leftpanel.jsp"></jsp:include>
        <div id="right-panel" class="right-panel">
        <jsp:include page="header.jsp"></jsp:include>

            <div class="breadcrumbs">
                <div class="breadcrumbs-inner">
                    <div class="row m-0">
                        <div class="col-sm-4">
                            <div class="page-header float-left">
                                <div class="page-title">
                                    <h1>Dashboard</h1>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="page-header float-right">
                                <div class="page-title">
                                    <ol class="breadcrumb text-right">
                                        <li><a href="${pageContext.request.contextPath}/home">Dashboard</a></li>
                                    <li><a href="${pageContext.request.contextPath}/changePassword">Change Password</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <aside class="profile-nav alt">
                            <section class="card">
                                <div class="card-header user-header alt bg-dark">
                                    <div class="media">
                                        <a href="#">
                                            <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="images/admin.jpg">
                                        </a>
                                        <div class="media-body">
                                            <h2 class="text-light display-6">${user.userName}</h2>
                                        </div>
                                    </div>
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <p style="color: red;" id="errorString"></p>

                                        <div class="col-lg-12">
                                            <div class="card-body card-block">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="col-lg-1"></div>
                                                        <div class="col-lg-3"><label>Re-training time: </label></div>
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-danger btn-number" id="btnMinusReTrain">
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </span>
                                                        <input type="text" id="reTrainTxt" name="reTrainTxt" class="form-control input-number col-lg-3" value="">
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-success btn-number" id="btnPlusReTrain">
                                                                <i class="fa fa-plus"></i>
                                                            </button>
                                                        </span>
                                                        <div class="col-lg-1"><label>minute</label></div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="col-lg-1"></div>
                                                        <div class="col-lg-3"><label>Division rate: </label></div>
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-danger btn-number" id="btnMinusDivision">
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </span>
                                                        <input type="text" id="divisionrateTxt" name="divisionrateTxt" class="form-control input-number col-lg-3" value="">
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-success btn-number" id="btnPlusDivision">
                                                                <i class="fa fa-plus"></i>
                                                            </button>
                                                        </span>
                                                        <div class="col-lg-1"><label>percent(%)</label></div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="col-lg-1"></div>
                                                        <div class="col-lg-3"><label>Fetch time: </label></div>
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-danger btn-number" id="btnMinusFetch">
                                                                <i class="fa fa-minus"></i>
                                                            </button>
                                                        </span>
                                                        <input type="text" id="fetchTimeTxt" name="fetchTimeTxt" class="form-control input-number col-lg-3" value="">
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-success btn-number" id="btnPlusFetch">
                                                                <i class="fa fa-plus"></i>
                                                            </button>
                                                        </span>
                                                        <div class="col-lg-1"><label>minute</label></div>
                                                    </div>
                                                </div>

                                                <div class="form-actions form-group">
                                                    <button type="button" value= "Change" class="btn btn-success btn-sm float-right" id="btnSaveSetting">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </section>
                        </aside>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

        <div class="clearfix"></div>
        <jsp:include page="footer.jsp"></jsp:include> 

    </div>
    <!-- Right Panel -->

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/systemsetting.js"></script>


    <script src="assets/js/lib/data-table/datatables.min.js"></script>
    <script src="assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="assets/js/lib/data-table/jszip.min.js"></script>
    <script src="assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="assets/js/init/datatables-init.js"></script>



</body>
</html>
</body>
</html>