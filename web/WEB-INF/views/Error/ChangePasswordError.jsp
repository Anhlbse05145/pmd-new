<%-- 
    Document   : ChangePasswordError
    Created on : Apr 10, 2019, 5:43:21 PM
    Author     : Mi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Change Password</title>
 </head>
 
 <body>
 
    <jsp:include page="header.jsp"></jsp:include>
    <jsp:include page="menu.jsp"></jsp:include>
    
    <h3>Change Password Error:</h3>
    
    <p style="color: red;">${errorString}</p>
    <a href="userInfo">User Info</a>
    
    <jsp:include page="footer.jsp"></jsp:include>
    
 </body>
</html>
