<%-- 
    Document   : footer
    Created on : Mar 30, 2019, 3:59:45 AM
    Author     : Mi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<footer class="site-footer">
    <div class="footer-inner bg-white">
        <div class="row">
            <div class="col-sm-6">
                Copyright &copy; 2019 PMD
            </div>
            <div class="col-sm-6 text-right">
                Designed by <a href="${pageContext.request.contextPath}">PMD</a>
            </div>
        </div>
    </div>
</footer>