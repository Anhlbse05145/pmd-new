<%-- 
    Document   : header
    Created on : Mar 30, 2019, 4:02:30 AM
    Author     : Mi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<header id="header" class="header">
    <div class="top-left">
        <div class="navbar-header">
            <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a>
            <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a>
            <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
        </div>
    </div>
    <div class="top-right">
        <div class="header-menu">
            <div class="user-area dropdown float-right">
                <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="user-avatar rounded-circle" src="images/admin.jpg" alt="User Avatar">
                </a>

                <div class="user-menu dropdown-menu">
                    
                    <a class="nav-link" href="${pageContext.request.contextPath}/userInfo"><i class="fa fa-user"></i>Hello ${loginedUser.userName}</a>
                    
                    <a class="nav-link" href="${pageContext.request.contextPath}/userInfo"><i class="fa fa-user"></i>My Profile</a>
                    
                    <a class="nav-link" href="${pageContext.request.contextPath}/changePassword"><i class="fa fa-key"></i>Change Pass</a>

                    <a class="nav-link" href="#"><i class="fa fa-cog"></i>Settings</a>

                    <a class="nav-link" href="${pageContext.request.contextPath}/logout"><i class="fa fa-power-off"></i>Logout</a>
                    
                    <a class="nav-link" href="${pageContext.request.contextPath}/login"><i class="fa fa-sign-in"></i>Login</a>
                    
                    <a class="nav-link" href="${pageContext.request.contextPath}/logout"><i class="fa fa-sign-in"></i>Logout</a>
                    
                    <a class="nav-link" href="${pageContext.request.contextPath}/ForgetPassword"><i class="fa fa-paper-plane"></i>Forget Pass</a>
                    
                </div>
            </div>

        </div>
    </div>
</header>