<%-- 
    Document   : EditInfoError
    Created on : Apr 11, 2019, 11:56:20 PM
    Author     : Mi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Change Password</title>
 </head>
 
 <body>
 
    <jsp:include page="header.jsp"></jsp:include>
    <jsp:include page="menu.jsp"></jsp:include>
    
    <h3>Edit Info Error:</h3>
    
    <p style="color: red;">${errorString}</p>
    <a href="userInfo">User Info</a>
    
    <jsp:include page="footer.jsp"></jsp:include>
    
 </body>
</html>
