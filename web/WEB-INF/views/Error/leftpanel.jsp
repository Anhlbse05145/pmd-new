<%-- 
    Document   : leftpanel
    Created on : Apr 18, 2019, 4:19:50 PM
    Author     : Mi-Pc
--%>

<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="${pageContext.request.contextPath}/home"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                </li>
                <li class="menu-title">Administrators</li><!-- /.menu-title -->
                <li>
                    <a href="${pageContext.request.contextPath}/userList"> <i class="menu-icon fa fa-cogs"></i>Account list</a>
                </li>

                <li class="menu-title">Customer</li><!-- /.menu-title -->
                <li>
                    <a href="${pageContext.request.contextPath}/listMailAccount"> <i class="menu-icon ti-email"></i>List Mail Account </a>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bar-chart"></i>Charts</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-line-chart"></i><a href="charts-chartjs.html">Chart JS</a></li>
                        <li><i class="menu-icon fa fa-area-chart"></i><a href="charts-flot.html">Flot Chart</a></li>
                        <li><i class="menu-icon fa fa-pie-chart"></i><a href="charts-peity.html">Peity Chart</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>