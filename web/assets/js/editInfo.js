/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$.ajax({
    method: "POST",
    url: getContextPath() + "GetUserInfoAPI",
    data: {
        type: "web",
        userNameInfo: localStorage.getItem("userNameInfo"),
        token: localStorage.getItem("token")
    }
}).done(function (data) {
    console.log(data);
    $("#nationSelect").val(data.nation);
    $("#phoneTxt").val("0" + data.mobile);
    $("#userIdTxt").val(data.userId);
});

$(document).ready(function () {
    $("#btnSave").click(function () {
        var nation = $("#nationSelect").val();
        var mobile = $("#phoneTxt").val();
        var userId = $("#userIdTxt").val();
        var confirmPass = $("#confirmPasswordTxt").val();

        var userInfo = {userId: userId, mobile: mobile, nation: nation, confirmPass: confirmPass};
        $.ajax({
            method: "POST",
            url: getContextPath() + "UpdateUserInfoAPI",
            data: {
                type: "web",
                userInfo: JSON.stringify(userInfo),
                token: localStorage.getItem("token")
            }
        }).done(function (data) {
            alert(data);
            if (data == "Update success!") {
                var token = localStorage.getItem("token");
                var href = getContextPath() + "userList?" + "token=" + token;
                loadPage(href);
            }
        });
    });
});
