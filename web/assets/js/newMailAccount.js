/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $("#btnSubmitNewPMA").click(function () {
        var mail = $("#mailTxt").val();
        var mailPass = $("#mailPasswordTxt").val();
        var accPass = $("#accPasswordTxt").val();
        var mailIsId = $("#mailIsIdSelect").val();
        var active;
        if ($("#cbPMAactive").is(':checked')) {
            active = true;
        } else {
            active = false;
        }

        var newMailAccount = {mail: mail, mailPass: mailPass, accPass: accPass, mailIsId: mailIsId, active: active};

        $.ajax({
            method: "POST",
            url: getContextPath() + "newMaillAccountAPI",
            data: {
                type: "web",
                newMailAccount: JSON.stringify(newMailAccount),
                token: localStorage.getItem("token")
            }
        }).done(function (data) {
            alert(data);
            if (data == "Create success!") {
                var token = localStorage.getItem("token");
                var href = getContextPath() + "listMailAccount?" + "token=" + token;
                loadPage(href);
            }
        });
    });
});

