/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$.ajax({
    method: "POST",
    url: getContextPath() + "GetPMAInfoAPI",
    data: {
        type: "web",
        pmaID: localStorage.getItem("pmaID"),
        token: localStorage.getItem("token")
    }
}).done(function (data) {
    console.log(data);
    $("#mailPMAtxt").val(data.mailAccount);
    $("#mailServerSelect").val(data.isId);
    if(data.active == true){
        $("#cbPMAactive").prop('checked', true);
    }
});

$(document).ready(function (){
    $("#btnSavePMA").click(function (){
        var pmaId = localStorage.getItem("pmaID");
        var mail = $("#mailPMAtxt").val();
        var isId = $("#mailServerSelect").val();
        var active;
        if($("#cbPMAactive").is(':checked')){
            active = true;
        } else{
            active = false;
        }
        var updateMailAcc = {userNAme: mail, isId: isId, active: active, pmaId: pmaId};
        $.ajax({
            method: "POST",
            url: getContextPath() + "UpdatePMAInfoAPI",
            data: {
                type: "web",
                updateMailAcc: JSON.stringify(updateMailAcc),
                token: localStorage.getItem("token")
            }
        }).done(function (data) {
            alert(data);
            if(data == "Update success!"){
                var token = localStorage.getItem("token");
                var href = getContextPath() + "listMailAccount?" + "token=" + token;
                loadPage(href);
            }
        });
        
    });
});
