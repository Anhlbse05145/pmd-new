/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var ctx = document.getElementById('horizontalChart').getContext('2d');

var tableAP = $("#bootstrap-data-table-AP").DataTable({
    lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
    ajax: {
        url: getContextPath() + "GetDataTable",
        type: "GET",
        data: {
            type: "web",
            token: localStorage.getItem("token")
        },
        dataSrc: "liDataTable",
    },
    columns: [
        {data: "TruePositive"},
        {data: "TrueNegative"},
        {data: "FalseNegative"},
        {data: "FalsePositive"},
        {data: "FalsePositiveRate"},
        {data: "FalseNegativeRate"},
        {data: "TruePositiveRate"},
        {data: "TrueNegativeRate"},
        {data: "Time"},
        {data: "Accuracy"}
    ],
    "searching": false,
    "ordering": false,
    "lengthChange": false,
    "info": false,
    "paging": false
});

$(document).ready(function (){
    
    $(document).ajaxStart(function () {
        $("#wait").css("display", "block");
    });
    $(document).ajaxComplete(function () {
        $("#wait").css("display", "none");
    });
    
    loadDataChart();
});

function objToStrMap(obj) {
    var strMap = new Map();
    for (var k of Object.keys(obj)) {
        strMap.set(k, obj[k]);
    }
    return strMap;
}

function loadDataChart() {
    $.ajax({
        method: "GET",
        url: getContextPath() + "GetDataChart",
        data: {
            type: "web",
            token: localStorage.getItem("token")
        }
    }).done(function (data) {
        
        var labels = new Array();
        var values = new Array();
        var dataMap = new Map();
        dataMap = objToStrMap(data);
        var iterator = dataMap.keys();
        let i = 0;
        while(true){
            i++;
            let iteratorResult = iterator.next();
            if(iteratorResult.done) break;
            labels[i] = iteratorResult.value;
            values[i] = dataMap.get(labels[i]);
        }

        drawChart(labels, values);
    });
}

function drawChart(labels, values) {
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: labels,
            datasets: [{
                    data: values,
                    label: 'Dataset 1',
                    backgroundColor: '#79CDCD',
                    borderColor: '#528B8B',
                    borderWidth: 1
                }]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: false
                        }
                    }]
            },
            animation: {
                duration: 0
            },
            title: {
                display: true,
                text: 'Traffic'
            },
            responsive: true
        }
    });
}
