/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var table = $("#bootstrap-data-table").DataTable({
    lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
    ajax: {
        url: getContextPath() + "UserListApi",
        type: "GET",
        data: {
            type: "web",
            token: localStorage.getItem("token")
        },
        dataSrc: "userInfos",
    },
    columns: [
        {data: "userId"},
        {data: "mail"},
        {data: "nation"},
        {data: "mobile"},
        {
            data: "role",
            render: function (data, type, row) {
                if ("admin" == data) {
                    return "<select onchange='updateRole(this)'><option value = '" + data + "' selected>Admin</option><option value = 'user'>User</option></select>";
                } else {
                    return "<select onchange='updateRole(this)'><option value = 'admin'>Admin</option><option value = '" + data + "' selected>User</option></select>";
                }
            }
        },
        {
            data: "",
            render: function (data, type, row) {
                return "<a href='#' onclick='resetPassword(this);return false;'>Reset</a>"
            }
        },
        {
            data: "",
            render: function (data, type, row) {
                return "<a href='#' onclick='editUserInfo(this);return false;'>Edit</a>"
            }
        },
        {
            data: "active",
            render: function (data, type, row) {
                if (data) {
                    return "<input type='checkbox' name='activeCB' onchange='updateActive(this);' value=' " + data + "' checked>";
                } else {
                    return "<input type='checkbox' name='activeCB' onchange='updateActive(this);' value=' " + data + "'>";
                }
            }
        }
    ]
});

function  updateActive(input) {
    var data = table.row($(input).closest('tr')).data();
    var activeInput = data.active;
    if (activeInput) {
        activeInput = false;
    } else {
        activeInput = true;
    }
    var userAccount = {userId: data.userId, active: activeInput};
    $.ajax({
        method: "POST",
        url: getContextPath() + "UpdateActiveAPI",
        data: {
            type: "web",
            userAccount: JSON.stringify(userAccount),
            token: localStorage.getItem("token"),
        }
    }).done(function (data) {
        alert(data);
        if ("Update success!" != data) {
            table.reload();
        }
    });
}

function updateRole(input) {
    var data = table.row($(input).closest('tr')).data();
    var roleInput = data.role;
    if (roleInput == "admin") {
        roleInput = "user";
    } else {
        roleInput = "admin";
    }
    var userRole = {userName: data.userName, role: roleInput};
    $.ajax({
        method: "POST",
        url: getContextPath() + "UpdateRoleAPI",
        data: {
            type: "web",
            userRole: JSON.stringify(userRole),
            token: localStorage.getItem("token"),
        }
    }).done(function (data) {
        alert(data);
        if ("Update success!" != data) {
            table.reload();
        }
    });
}

function resetPassword(input) {
    var data = table.row($(input).closest('tr')).data();
    var userId = data.userId;
    $.ajax({
        method: "POST",
        url: getContextPath() + "ResetPasswordAPI",
        data: {
            type: "web",
            userId: userId,
            token: localStorage.getItem("token"),
        }
    }).done(function (data) {
        alert(data);
    });
}

function editUserInfo(input) {
    var data = table.row($(input).closest('tr')).data();
    var userName = data.userName;
    localStorage.setItem("userNameInfo", userName);
    var token = localStorage.getItem("token");
    var href = getContextPath() + "editInfoUser?" + "token=" + token;
    loadPage(href);
}
