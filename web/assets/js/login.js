/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    "use strict";

    $("#signInBt").click(function () {
        var userNameTxt = $("#userName").val();
        var passwordTxt = $("#password").val();
        var userLogin = {userName: userNameTxt, password: passwordTxt};
        $.ajax({
            method: "POST",
            url: getContextPath() + "LoginAPI",
            data: {
                type: "extension",
                userInfo: JSON.stringify(userLogin)
            }
        }).done(function (data) {
            console.log(data);
            if (data.loggedIn) {
                localStorage.setItem("token", data.token);
                localStorage.setItem("role", data.role);
                if(data.role == "admin"){
                    var href = getContextPath() + "home?" + "token=" + data.token;
                } else{
                    var href = getContextPath() + "listMailAccount?" + "token=" + data.token;
                }
                loadPage(href);
            } else {
                console.log(data.message);
                $("#messageLogin").text(data.message);
            }
        });
    });
});
