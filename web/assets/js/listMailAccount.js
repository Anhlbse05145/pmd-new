/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tablePMA = $("#bootstrap-data-table-PMA").DataTable({
    lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
    ajax: {
        url: getContextPath() + "listMailAccountAPI",
        type: "GET",
        data: {
            type: "web",
            token: localStorage.getItem("token")
        },
        dataSrc: "listMailAcc",
    },
    columns: [
        {data: "pmaID"},
        {data: "mailAccount"},
        {
            data: "isId",
            render: function (data, type, row) {
                if (data == 1) {
                    return "Yahoo";
                } else if (data == 2) {
                    return "GMail";
                } else {
                    return "No mail server!";
                }
            }
        },
        {
            data: "",
            render: function (data, type, row) {
                return "<a href='#' onclick='editMailAccount(this);return false;'>Edit</a>"
            }
        },
        {
            data: "active",
            render: function (data, type, row) {
                if (data) {
                    return "<input type='checkbox' name='activeCB' onchange='updateActivePMA(this);' value=' " + data + "' checked>";
                } else {
                    return "<input type='checkbox' name='activeCB' onchange='updateActivePMA(this);' value=' " + data + "'>";
                }
            }
        },
        {data: "isVerify"}
    ]
});

function editMailAccount(input) {
    var data = tablePMA.row($(input).closest('tr')).data();
    var pmaID = data.pmaID;
    localStorage.setItem("pmaID", pmaID);
    var token = localStorage.getItem("token");
    var href = getContextPath() + "changeMailAccount?" + "token=" + token;
    loadPage(href);
}

function updateActivePMA(input) {
    var data = tablePMA.row($(input).closest('tr')).data();
    var activeInput = data.active;
    if (activeInput) {
        activeInput = false;
    } else {
        activeInput = true;
    }
    var updateActivePMA = {pmaID: data.pmaID, active: activeInput};
    $.ajax({
        method: "POST",
        url: getContextPath() + "UpdateActivePmaAPI",
        data: {
            type: "web",
            updateActivePMA: JSON.stringify(updateActivePMA),
            token: localStorage.getItem("token"),
        }
    }).done(function (data) {
        alert(data);
        if ("Update success!" != data) {
            tablePMA.reload();
        }
    });
}

$(document).ready(function () {
    $("#btnNewMailAcc").click(function () {
        var token = localStorage.getItem("token");
        var href = getContextPath() + "newMailAccount?" + "token=" + token;
        loadPage(href);
    });
});
