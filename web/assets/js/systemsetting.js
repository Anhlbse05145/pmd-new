/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$.ajax({
    method: "POST",
    url: getContextPath() + "GetSystemSetting",
    data: {
        type: "web",
        token: localStorage.getItem("token")
    }
}).done(function (data) {
    console.log(data);
    if (data.errorString == "Get data success!") {
        var setting = data.systemSettting;
        console.log(setting);
        $("#reTrainTxt").val(setting.retraining_time);
        $("#divisionrateTxt").val(setting.test_data_size);
        $("#fetchTimeTxt").val(setting.fetch_time);
    } else {
        alert(data.errorString)
    }

});

$(document).ready(function () {
    $("#btnMinusReTrain").click(function () {
        var result = minusInt("reTrainTxt", 1);
        $("#reTrainTxt").val(result);
    });
    $("#btnMinusDivision").click(function () {
        var result = minusFloat("divisionrateTxt", 1);
        $("#divisionrateTxt").val(result);
    });
    $("#btnMinusFetch").click(function () {
        var result = minusInt("fetchTimeTxt", 1);
        $("#fetchTimeTxt").val(result);
    });
    $("#btnPlusReTrain").click(function () {
        var result = plusInt("reTrainTxt", 1);
        $("#reTrainTxt").val(result);
    });
    $("#btnPlusDivision").click(function () {
        var result = plusFloat("divisionrateTxt", 1);
        $("#divisionrateTxt").val(result);
    });
    $("#btnPlusFetch").click(function () {
        var result = plusInt("fetchTimeTxt", 1);
        $("#fetchTimeTxt").val(result);
    });
    $("#btnSaveSetting").click(function () {
        var reTrain = $("#reTrainTxt").val();
        var division = $("#divisionrateTxt").val();
        var fetch = $("#fetchTimeTxt").val();
        var setting = {retraining_time: reTrain, test_data_size: division, fetch_time: fetch};
        $.ajax({
            method: "POST",
            url: getContextPath() + "UpdateSystemSetting",
            data: {
                type: "web",
                setting: JSON.stringify(setting),
                token: localStorage.getItem("token")
            }
        }).done(function (data) {
            alert(data);
        });
    });
});

function minusInt(id, number) {
    var value = parseInt($("#" + id).val());
    var result = value - number;
    return result;
}

function plusInt(id, number) {
    var value = parseInt($("#" + id).val());
    var result = value + number;
    return result;
}

function minusFloat(id, number) {
    var value = parseFloat($("#" + id).val());
    var result = value - number;
    return result.toFixed(2);
}

function plusFloat(id, number) {
    var value = parseFloat($("#" + id).val());
    var result = value + number;
    return result.toFixed(2);
}

