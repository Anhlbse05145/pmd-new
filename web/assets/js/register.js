/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $("#btnRegister").click(function () {
        var userNameTxt = $("#userNameTxt").val();
        var passwordTxt = $("#passwordTxt").val();
        var confirmPasswordTxt = $("#confirmPasswordTxt").val();
        var mailTxt = $("#mailTxt").val();
        var mobileTxt = $("#mobileTxt").val();
        var nationTxt = $("#nationTxt").val();

        var userRegister = {
            userName: userNameTxt,
            password: passwordTxt,
            confirmPassword: confirmPasswordTxt,
            mail: mailTxt,
            mobile: mobileTxt,
            nation: nationTxt
        };

        $.ajax({
            method: "POST",
            url: getContextPath() + "RegisterUser",
            data: {
                type: "web",
                userRegister: JSON.stringify(userRegister),
                token: localStorage.getItem("token")
            }
        }).done(function (data) {
            alert(data);
            if(data == "Register success!"){
                var href = getContextPath() + "login";
                loadPage(href);
            }
        });
    });
});
