/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $("#btnChange").click(function () {
        var currentPasswordTxt = $("#currentPasswordTxt").val();
        var newPasswordTxt = $("#newPasswordTxt").val();
        var confirmPasswordTxt = $("#confirmPasswordTxt").val();
        var changePass = {
            currentPassword: currentPasswordTxt,
            newPassword: newPasswordTxt,
            confirmPassword: confirmPasswordTxt
        };
        $.ajax({
            method: "POST",
            url: getContextPath() + "ChangePassAPI",
            data: {
                type: "web",
                changePass: JSON.stringify(changePass),
                token: localStorage.getItem("token")
            }
        }).done(function (data) {
            alert(data);
            if(data == "Change password success!"){
                var href = getContextPath() + "login";
                loadPage(href);
            }
        });
    });
});
