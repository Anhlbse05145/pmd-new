
function getContextPath() {
    var pathNameURL = window.location.pathname.split("/");
    var pathName = "/" + pathNameURL[1] + "/";
    return window.location.protocol + "//" + window.location.host + pathName;
}

function loadPage(href) {
    window.location.href = href;
}

$(document).ready(function ($) {
    
    if(localStorage.getItem("role") == "admin"){
        $("#listMailAcchref").hide();
    } else{
        $("#dashboardhref").hide();
        $("#userListhref").hide();
        $("#systemSettinghref").hide();
    }

    $(document).ajaxStart(function () {
        $("#wait").css("display", "block");
    });
    $(document).ajaxComplete(function () {
        $("#wait").css("display", "none");
    });

    "use strict";

    [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
        new SelectFx(el);
    });

    $('.selectpicker').selectpicker;

    $('.search-trigger').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $('.search-trigger').parent('.header-left').addClass('open');
    });

    $('.search-close').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $('.search-trigger').parent('.header-left').removeClass('open');
    });

    $('.equal-height').matchHeight({
        property: 'max-height'
    });

    // Counter Number
    $('.count').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 3000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });

    // Menu Trigger
    $('#menuToggle').on('click', function (event) {
        var windowWidth = $(window).width();
        if (windowWidth < 1010) {
            $('body').removeClass('open');
            if (windowWidth < 760) {
                $('#left-panel').slideToggle();
            } else {
                $('#left-panel').toggleClass('open-menu');
            }
        } else {
            $('body').toggleClass('open');
            $('#left-panel').removeClass('open-menu');
        }

    });

    $(".menu-item-has-children.dropdown").each(function () {
        $(this).on('click', function () {
            var $temp_text = $(this).children('.dropdown-toggle').html();
            $(this).children('.sub-menu').prepend('<li class="subtitle">' + $temp_text + '</li>');
        });
    });

    // Load Resize 
    $(window).on("load resize", function (event) {
        var windowWidth = $(window).width();
        if (windowWidth < 1010) {
            $('body').addClass('small-device');
        } else {
            $('body').removeClass('small-device');
        }
    });

    $("#userListhref").click(function () {
        var token = localStorage.getItem("token");
        var href = getContextPath() + "userList?" + "token=" + token;
        loadPage(href);
    });

    $("#changePasshref").click(function () {
        var token = localStorage.getItem("token");
        var href = getContextPath() + "changePassword?" + "token=" + token;
        loadPage(href);
    });

    $("#dashboardhref").click(function () {
        var token = localStorage.getItem("token");
        var href = getContextPath() + "home?" + "token=" + token;
        loadPage(href);
    });

    $("#listMailAcchref").click(function () {
        var token = localStorage.getItem("token");
        var href = getContextPath() + "listMailAccount?" + "token=" + token;
        loadPage(href);
    });

    $("#systemSettinghref").click(function () {
        var token = localStorage.getItem("token");
        var href = getContextPath() + "SystemSetting?" + "token=" + token;
        loadPage(href);
    });

});

function logout() {
    localStorage.removeItem("token");
    var href = getContextPath() + "login";
    loadPage(href);
}