<%-- 
    Document   : menu
    Created on : Mar 30, 2019, 4:00:55 AM
    Author     : Mi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div style="padding: 5px;">
 
   <a href="${pageContext.request.contextPath}/">Home</a>
   |
   <a href="${pageContext.request.contextPath}/userList">All User List</a>
   |
   <a href="${pageContext.request.contextPath}/userInfo">My Account Info</a>
   |
   <a href="${pageContext.request.contextPath}/login">Login</a>
    
</div>  