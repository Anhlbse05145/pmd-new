<%-- 
    Document   : ForgetPassword
    Created on : Apr 13, 2019, 3:21:18 PM
    Author     : Mi
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Forget Password</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="Login page">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="images/favicon.png">
        <link rel="shortcut icon" href="images/favicon.png">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
        <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    </head>
    <body class="bg-light">

        <div class="sufee-login d-flex align-content-center flex-wrap">
            <div class="container">
                <div class="login-content">
                    <div class="login-logo">
                        <a href="${pageContext.request.contextPath}/login">
                            <img class="align-content" src="images/logo.png" alt="">
                        </a>
                    </div>
                    <div class="login-form">
                        <form method="POST" action="${pageContext.request.contextPath}/ForgetPassword">
                            <h3>Forget Password!!!</h3>
                            <br>
                            <p>Please fill out the information to get password.</p>
                            <p style="color: red;">${errorString}</p>
                            <p style="color: black;">${note}</p>
                            
                            <div class="form-group">
                                <label>User Name</label>
                                <input type="text" class="form-control" id="userNameTxt"name="userName">
                            </div>
                            
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" id="mailTxt" name="mail">
                            </div>
                            
                            <button type="button" class="btn btn-success btn-flat m-b-30 m-t-30" id="btnSendMail">Send Mail</button>
                            <div class="register-link m-t-15 text-center">
                                <p>Don't have account ? <a href="${pageContext.request.contextPath}/register"> Sign Up Here</a><a href="${pageContext.request.contextPath}/login"> | Login</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/forgetPassword.js"></script>

    </body>
</html>