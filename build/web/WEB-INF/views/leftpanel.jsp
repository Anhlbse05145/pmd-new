<%-- 
    Document   : leftpanel
    Created on : Apr 18, 2019, 4:19:50 PM
    Author     : Mi-Pc
--%>

<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="#" id="dashboardhref"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                </li>
                <li>
                    <a href="#" id="userListhref"> <i class="menu-icon fa fa-cogs"></i>Account list</a>
                </li>
                <li>
                    <a href="#" id="systemSettinghref"> <i class="menu-icon fa fa-cogs"></i>System Setting</a>
                </li>
                <li>
                    <a href="#" id="listMailAcchref"> <i class="menu-icon ti-email"></i>List Mail Account </a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>