<%-- 
    Document   : ResetPasswordError
    Created on : Apr 1, 2019, 12:45:18 PM
    Author     : Mi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>User List Error</title>
 </head>
 
 <body>
 
    <jsp:include page="header.jsp"></jsp:include>
    <jsp:include page="menu.jsp"></jsp:include>
    
    <h3>User List Error</h3>
    
    <p style="color: red;">${errorString}</p>
    <a href="userInfo">User List</a>
    
    <jsp:include page="footer.jsp"></jsp:include>
    
 </body>
</html>
