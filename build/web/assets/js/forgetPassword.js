/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $("#btnSendMail").click(function () {

        var userName = $("#userNameTxt").val();
        var mail = $("#mailTxt").val();
        var forgetPassword = {userName : userName, mail : mail};

        $.ajax({
            method: "POST",
            url: getContextPath() + "ForgetPasswordAPI",
            data: {
                type: "web",
                forgetPassword: JSON.stringify(forgetPassword),
                token: localStorage.getItem("token")
            }
        }).done(function (data) {
            alert(data);
        });
    });
});

